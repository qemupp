CXXFLAGS = -g -Wall -O -std=gnu++0x
CXXFLAGS += -MMD -MP -MF $@.d
CXXFLAGS += -Iinclude

OBJ := time/timer.o time/test-time-source.o
OBJ += device/device.o
# device/regio.o
OBJ += pc/i8254.o pc/mc146818a.o pc/i8042.o
#OBJ += pc/i8042.o pc/southbridge.o
OBJ += ps2/ps2.o ps2/ps2_keyboard.o ps2/ps2_mouse.o
OBJ += marshal/marshal.o marshal/xmlwriter.o
OBJ += test/timelib.o

PROGS := test-i8254 test-mc146818a test-timer test-marshal test-array test-kbd
PROGS += test-i8042 #test-southbridge test-regio

all: $(PROGS)

%.o: %.cpp
	$(CXX) -c -o $@ $(CXXFLAGS) $<

test-%: test/test-%.o $(OBJ)
	$(CXX) -o $@ $(LDFLAGS) $^ $(LDLIBS)

docs: Doxyfile
	doxygen $<

check: $(PROGS)
	@for i in $(PROGS); do \
	    ./$$i; \
	done

clean:
	$(RM) */*.o */*~ $(PROGS)

depclean: clean
	$(RM) */*.o.d

.PHONY: clean docs

-include */*.d
