#include "device.hpp"

Device::Device(void) : vcc_wire(this->vcc)
{
    std::function<void (void)> fn;

    /* We use vcc_wire instead of vcc directly to avoid dealing with plug
     * events */
    fn = std::bind(&Device::realize, this);
    this->vcc_wire.out.connect(Pin::RISING_EDGE, fn);

    fn = std::bind(&Device::reset, this);
    this->vcc_wire.out.connect(Pin::FALLING_EDGE, fn);
}

Device::~Device(void)
{
}

void Device::reset(void)
{
}

void Device::realize(void)
{
}

void Device::add_subordinate(Device *other)
{
    other->vcc = &this->vcc_wire.out;
}
