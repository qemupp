#include "io.hpp"

#include <stdio.h>

Pin::Pin(void) :
    state(false)
{
}

Pin::Pin(bool init) :
    state(init)
{
}

void Pin::raise(void)
{
    this->set(true);
}

void Pin::lower(void)
{
    this->set(false);
}

void Pin::set(bool value)
{
    if (this->notifier && this->state != value) {
        this->state = value;
        this->notifier(value);
    } else {
        this->state = value;
    }
}

bool Pin::get(void)
{
    return this->state;
}

void Pin::set_edge_notifier(std::function<void (bool)> func)
{
    this->notifier = func;
}

void Pin::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "Pin");
    marshal(m, "state", &this->state);
    m->end_struct();
}
