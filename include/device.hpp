#ifndef QEMU_DEVICE_HPP
#define QEMU_DEVICE_HPP

#include "pin.hpp"
#include "wire.hpp"

class Marshaller;

/**
 * The device base class.
 *
 * This is the common base class that represents all other devices.  There is
 * very little in this class and that's by design.  Whatever interfaces are
 * here MUST make sense for any device and there's very few common interfaces
 * that makes sense for ALL devices.
 *
 * Do not introduce an "optional" interface here.  Instead, introduce another
 * interface or subclass and use that.
 */

class Device
{
public:
    Device(void);
    virtual ~Device(void) = 0;

    /**
     * Pickle a device to a marshaller
     */
    virtual void pickle(Marshaller *m, const char *name) = 0;

    /**
     * Reset the device
     */
    virtual void reset(void);

    /**
     * Realize the device.  This is the point in the object's life cycle when
     * all of the children has been connected and the device should expect to
     * be interacted with.  The default behavior is to invoke reset.
     *
     * If a device has Plugs and does not support hot plug, it should lock its
     * Plugs here.
     */
    virtual void realize(void);

    /**
     * The Vcc input for the device.  This might seem like an unusually low
     * level to model but it turns out to make a lot of sense.  The power pin
     * can never form a loop and since machines typically have a central power
     * supply, the result is a tree with a single root.
     *
     * The pin's semantics are as follows: the device starts out with vcc = 0,
     * which is fundamentally the same as vcc being unplugged.  Once vcc = 1,
     * the device is powered on.
     *
     * The transitions between vcc=0 and vcc=1 are interesting.  The rising
     * edge (power-off to power-on) maps to the realize event.  This is the
     * moment before the device is guest visible that can be used to validate
     * the state.  By default, the Device constructor maps the vcc rising edge
     * notification to the realize() event.
     *
     * The falling edge (power-on to power-off) maps to the reset() event.  This
     * is when the guest loses power and therefore needs to reset it's internal
     * state.
     *
     * A big advantage of this model is that it allows for multiple RESET pins
     * to be modelled.  For devices that distinguish between soft resets and
     * hard resets, the device can add another pin that can be controlled
     * entirely independently of vcc.
     */
    Socket<Pin> vcc;

protected:
    Wire vcc_wire;

    void add_subordinate(Device *other);
};

#endif
