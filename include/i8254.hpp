#ifndef I8254_HPP
#define I8254_HPP

#include "device.hpp"
#include "marshal.hpp"
#include "timer.hpp"
#include "util.hpp"

#define PIT_FREQ 1193182

class PITChannel : public Device
{
public:
    PITChannel(void);

    void write_back_command(uint8_t val);
    void write_control(uint8_t val);
    void write_counter(uint8_t val);
    uint8_t read(void);

    void pickle(Marshaller *m, const char *name);
    void reset(void);

    void set_drift_mode(DriftMode mode);

    Pin irq;

private:
    int64_t get_next_transition_time(uint64_t current_time);
    void irq_timer_update(uint64_t current_time);
    void on_irq_timer(void);
    int get_out1(int64_t current_time);
    void load_count(int val);
    void latch_count(void);
    uint16_t get_count(void);
    void update_irq(uint64_t current_time);

    uint32_t count; /* can be 65536 */
    uint16_t latched_count;
    uint8_t count_latched;
    uint8_t status_latched;
    uint8_t status;
    uint8_t read_state;
    uint8_t write_state;
    uint8_t write_latch;
    uint8_t rw_mode;
    uint8_t mode;
    uint8_t bcd; /* not supported */
    int64_t count_load_time;
    /* irq handling */
    int64_t next_transition_time;
    Timer irq_timer;
    int64_t missed_ticks;

    DriftMode drift_mode;
};

class PIT : public Device
{
public:
    PIT(DriftMode mode = DRIFT_DROP);

    void pickle(Marshaller *m, const char *name);

    void write(uint8_t addr, uint8_t val);
    uint8_t read(uint8_t addr);

    Pin &get_irq(void);

private:
    Array<PITChannel, 3> channels;
};

#endif
