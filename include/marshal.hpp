#ifndef MARSHAL_HPP
#define MARSHAL_HPP

#include <inttypes.h>

#include "util.hpp"

#include <vector>

/**
 * Object marshalling support
 *
 * This modules provides a mechanism to serialize arbitrary types.  The
 * Marshaller class provides an interface to marshal fundamental types and then
 * the marshal() function provides specialized functions to marshal complex
 * types in terms of the fundamental type interfaces.
 *
 * The Marshaller class also provides interfaces to describe structures and
 * arrays.
 *
 * There are two ways an type can support the marshalling interface.  For
 * objects, if the object implements a pickle method, that method will be
 * called to marshal the object.  If a pickle method cannot be added, then
 * the marshal function should be overloaded for the type.
 *
 * The marshal or pickle function should marshal every member of an object.
 * It should not visit nodes conditional or have any type of logic in the
 * marshaling function.
 *
 * If there's a desire to suppress fields or otherwise modify the marshalled
 * data, it should be done by using a DOMMarshaller.
 */

class Marshaller
{
public:
    virtual void marshal_int8(const char *name, int8_t *obj) = 0;
    virtual void marshal_int16(const char *name, int16_t *obj) = 0;
    virtual void marshal_int32(const char *name, int32_t *obj) = 0;
    virtual void marshal_int64(const char *name, int64_t *obj) = 0;

    virtual void marshal_uint8(const char *name, uint8_t *obj) = 0;
    virtual void marshal_uint16(const char *name, uint16_t *obj) = 0;
    virtual void marshal_uint32(const char *name, uint32_t *obj) = 0;
    virtual void marshal_uint64(const char *name, uint64_t *obj) = 0;

    virtual void marshal_bool(const char *name, bool *obj) = 0;

    virtual void start_struct(const char *name, const char *type) = 0;
    virtual void end_struct(void) = 0;

    virtual void start_array(const char *name) = 0;
    virtual void end_array(void) = 0;
};

void marshal(Marshaller *m, const char *name, int8_t *obj);
void marshal(Marshaller *m, const char *name, int16_t *obj);
void marshal(Marshaller *m, const char *name, int32_t *obj);
void marshal(Marshaller *m, const char *name, int64_t *obj);

void marshal(Marshaller *m, const char *name, uint8_t *obj);
void marshal(Marshaller *m, const char *name, uint16_t *obj);
void marshal(Marshaller *m, const char *name, uint32_t *obj);
void marshal(Marshaller *m, const char *name, uint64_t *obj);

void marshal(Marshaller *m, const char *name, bool *obj);

template <typename T>
void marshal(Marshaller *m, const char *name, T *obj)
{
    obj->pickle(m, name);
}

template <typename T, size_t N>
void marshal(Marshaller *m, const char *name, Array<T, N> *array)
{
    m->start_array(name);
    for (size_t i = 0; i < array->size(); i++) {
        marshal(m, NULL, &array->at(i));
    }
    m->end_array();
}

template <typename T>
void marshal(Marshaller *m, const char *name, std::vector<T> *vector)
{
    uint64_t size = vector->size();

    m->start_struct(name, "std::vector");
    marshal(m, "size", &size);
    vector->resize(size);

    m->start_array(name);
    for (size_t i = 0; i < size; i++) {
        marshal(m, NULL, &vector[i]);
    }
    m->end_array();
    m->end_struct();
}

#endif
