#ifndef MC146818A_HPP
#define MC146818A_HPP

#include <time.h>

#include "marshal.hpp"
#include "device.hpp"
#include "timer.hpp"
#include "pin.hpp"
#include "util.hpp"

/**
 * Motorola 146818A  Real Time Clock/CMOS
 *
 * This modules implements emulation for the RTC device.
 */

class RTC : public Device
{
public:
    /* Drift mode refers to the periodic timer (mode 2) */
    RTC(int32_t base_year, struct tm *tm, DriftMode mode=DRIFT_REINJECT_FAST);

    /* Direct cmos interface */
    uint8_t cmos_read(uint8_t index);
    void cmos_write(uint8_t index, uint8_t data);

    /* Latched interface provided by mc146818a */
    uint8_t read(uint8_t addr);
    void write(uint8_t addr, uint8_t data);

    void pickle(Marshaller *m, const char *name);
    void reset(void);

    Pin irq;

private:
    int from_bcd(int a);
    int to_bcd(int a);

    void update_timer(int64_t current_time);
    void set_time(void);
    void copy_date(void);

    void on_periodic(void);
    void on_second(void);
    void on_second2(void);

    int32_t base_year;

    Array<uint8_t, 128> cmos_data;
    struct tm current_tm;

    int64_t next_periodic_time;
    int64_t next_second_time;

    Timer periodic_timer;
    Timer second_timer;
    Timer second_timer2;

    DriftMode drift_mode;
    int64_t missed_ticks;

    uint8_t cmos_index;
};

void marshal(Marshaller *m, const char *name, struct tm *obj);

#endif
