#ifndef NOTIFY_HPP
#define NOTIFY_HPP

#include <functional>
#include <list>
#include <utility>

typedef std::function<void (void)> Notifier;
typedef std::list<std::pair<int, Notifier> > NotifierList;
typedef NotifierList::iterator NotifierHandle;

class Notifiable
{
public:
    void disconnect(NotifierHandle handle) {
        this->notifier_list.erase(handle);
    }

protected:
    NotifierHandle connect(int type, Notifier notifier) {
        NotifierList &nl = this->notifier_list;
        std::pair<int, Notifier> item(type, notifier);
        return nl.insert(nl.end(), item);
    }

    void notify(int event) {
        NotifierList &nl = this->notifier_list;
        NotifierList::iterator i;

        for (i = nl.begin(); i != nl.end(); ++i) {
            if (i->first == event) {
                i->second();
            }
        }
    }

private:
    NotifierList notifier_list;
};

#endif
