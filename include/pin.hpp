#ifndef PIN_HPP
#define PIN_HPP

#include "notify.hpp"
#include "marshal.hpp"

class Pin : public Notifiable
{
public:
    Pin(bool value=false) : value(value) {
    }

    enum Event {
        RISING_EDGE,
        FALLING_EDGE,
    };

    NotifierHandle connect(Event type, Notifier notifier) {
        return Notifiable::connect(type, notifier);
    }

    bool get(void) const {
        return this->value;
    }

    void set(bool value) {
        if (value && !this->value) {
            this->value = value;
            this->notify(RISING_EDGE);
        } else if (!value && this->value) {
            this->value = value;
            this->notify(FALLING_EDGE);
        }
    }

    void lower(void) {
        this->set(false);
    }

    void raise(void) {
        this->set(true);
    }

    void pickle(Marshaller *m, const char *name) {
        bool value = this->get();
        m->start_struct("Pin", name);
        marshal(m, "value", &value);
        m->end_struct();
        this->set(this->value);
    }

private:
    bool value;
};

#endif
