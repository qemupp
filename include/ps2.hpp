#ifndef PS2_HPP
#define PS2_HPP

#include "device.hpp"
#include "pin.hpp"
#include "util.hpp"

#define PS2_QUEUE_SIZE 256

class PS2Controller;
class PS2Device;

class PS2Controller
{
public:
    virtual void data_ready(PS2Device *device) = 0;
};

class PS2Device : public Device
{
public:
    virtual ~PS2Device(void) = 0;

    virtual void write(int val) = 0;
    uint32_t read(void);
    bool can_read(void);

    virtual void pickle(Marshaller *m, const char *name);
    virtual void reset(void);

    void queue(int b);

    Socket<PS2Controller> bus;

protected:
    bool queue_has_space(int count);

    int32_t write_cmd;

private:
    Array<uint8_t, PS2_QUEUE_SIZE> data;
    int rptr, wptr, count;
};

#endif
