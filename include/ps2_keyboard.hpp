#ifndef PS2_KEYBOARD_HPP
#define PS2_KEYBOARD_HPP

#include "ps2.hpp"

class PS2Keyboard : public PS2Device
{
public:
    PS2Keyboard(void);
    void write(int val);
    void set_translation(int val);

    void reset(void);
    void pickle(Marshaller *m, const char *name);

    void put_keycode(int keycode);

    Pin scroll_lock_led;
    Pin num_lock_led;
    Pin caps_lock_led;

private:
    void reset_keyboard(void);

    int scan_enabled;
    int translate;
    int scancode_set;
};

#endif
