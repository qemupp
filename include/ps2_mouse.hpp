#ifndef PS2_MOUSE_HPP
#define PS2_MOUSE_HPP

#include "ps2.hpp"

class PS2Mouse : public PS2Device
{
public:
    PS2Mouse(void);

    void write(int val);
    void event(int dx, int dy, int dz, int buttons_state);

    void reset(void);

    void pickle(Marshaller *m, const char *name);

private:
    void send_packet(void);

    uint8_t status;
    uint8_t resolution;
    uint8_t sample_rate;
    uint8_t wrap;
    uint8_t type; /* 0 = PS2, 3 = IMPS/2, 4 = IMEX */
    uint8_t detect_state;
    int dx; /* current values, needed for 'poll' mode */
    int dy;
    int dz;
    uint8_t buttons;
};

#endif
