#ifndef REGIO_HPP
#define REGIO_HPP

#include <stdint.h>

#include "util.hpp"

enum IOFlags
{
    READWRITE,
    READONLY,
    WRITECLEAR,
};

class RegisterIO
{
public:
    RegisterIO(uint32_t addr, unsigned len, uint32_t *val, bool read);

    template <typename T>
    bool io(T *field, uint32_t base_addr,
	    IOFlags flags=READWRITE, uint32_t rmask=0);

    template <typename T, size_t N>
    bool io(Array<T, N> *array, uint32_t base_addr,
	    IOFlags flags=READWRITE, uint32_t rmask=0);

private:
    uint32_t addr;
    unsigned len;
    uint32_t *val;
    bool read;
};

template <typename T>
bool RegisterIO::io(T *field, uint32_t base_addr, IOFlags flags,
		    uint32_t rmask)
{
    uint32_t offset, size, mask;
    uint32_t value = *val;

    if (this->addr < base_addr || this->addr >= (base_addr + sizeof(T))) {
        return false;
    }

    offset = (this->addr - base_addr) * 8;
    size = MIN(sizeof(T) - offset, this->len);
    mask = ((1ULL << (size * 8)) - 1);

    /* little endian */
    if (this->read) {
        value = *field >> offset;
        value &= mask;
    } else if (flags == WRITECLEAR) {
        value &= mask;
        *field &= ~(value << offset) | rmask;
    } else if (flags != READONLY) {
        value &= mask;
        *field &= ~(mask << offset) | rmask;
        *field |= (value << offset) & ~rmask;
    }

    if (this->read) {
        *val = value;
    }

    return true;
}

template <typename T, size_t N>
bool RegisterIO::io(Array<T, N> *array, uint32_t base_addr,
		    IOFlags flags, uint32_t rmask)
{
    bool ret = false;

    for (size_t i = 0; i < N; ++i) {
        if (this->io(&array[i], base_addr + (i * sizeof(T)), flags)) {
            ret = true;
        }
    }

    return ret;
}

#endif
