#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <exception>

#include "marshal.hpp"
#include "notify.hpp"

struct LockedPlugException : public std::exception {
    const char *what(void) const throw() {
        return "attempt to modify a locked plug";
    }
};

struct NullPointerException : public std::exception {
    const char *what(void) const throw() {
        return "dereference of a NULL pointer";
    }
};

template <typename PlugType>
class Socket : public Notifiable
{
public:
    enum Event {
        PLUGGED,
        UNPLUGGED,
    };

    Socket(PlugType *obj=NULL)
        : obj(obj), lock_flag(false) {
    }

    NotifierHandle connect(Event type, std::function<void (void)> notifier) {
        return Notifiable::connect(type, notifier);
    }

    bool locked(void) {
        return this->lock_flag;
    }

    bool empty(void) {
        return !this->obj;
    }

    void lock(void) {
        this->lock_flag = true;
    }

    void unlock(void) {
        this->lock_flag = false;
    }

    PlugType *get(void) {
        if (this->obj == NULL) {
            throw new NullPointerException();
        }
        return this->obj;
    }

    void set(PlugType *obj) {
        if (this->locked()) {
            throw new LockedPlugException();
        }
	if (obj == NULL) {
	  throw new NullPointerException();
	}
        this->obj = obj;
        this->notify(PLUGGED);
    }

    void eject(void) {
        if (this->locked()) {
            throw new LockedPlugException();
        }
        this->obj = NULL;
        this->notify(UNPLUGGED);
    }

    operator bool(void) {
        return !!this->obj;
    }

    Socket &operator=(PlugType *obj) {
        this->set(obj);
        return *this;
    }

    PlugType *operator->(void) {
        return this->get();
    }

    PlugType &operator*(void) {
        return *this->get();
    }

    void pickle(Marshaller *m, const char *name) {
        m->start_struct("Plug", name);
        if (this->obj) {
            marshal(m, "obj", this->obj);
        }
        marshal(m, "lock_flag", &this->lock_flag);
        m->end_struct();
    }

private:
    PlugType *obj;
    bool lock_flag;
};

#endif
