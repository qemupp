#ifndef SOUTH_BRIDGE_HPP
#define SOUTH_BRIDGE_HPP

#include "device.hpp"
#include "i8042.hpp"
#include "mc146818a.hpp"
#include "i8254.hpp"

class SouthBridge : public Device
{
public:
    SouthBridge(int32_t base_year, struct tm *tm, DriftMode mode);

    void write_port(uint16_t addr, uint32_t val);
    uint32_t read_port(uint16_t addr);

    void reset(void);
    void pickle(Marshaller *m, const char *name);

    I8042 i8042;
    RTC rtc;
    PIT pit;
};

#endif
