#ifndef TEST_TIME_SOURCE_HPP
#define TEST_TIME_SOURCE_HPP

#include "timer.hpp"
#include <list>

/**
 * This class provides a TimeSource that can be precisely controlled.  It's very
 * useful for writing test cases where you need to create precise scenarios
 * that are timing dependent.
 */

class TestTimeSource : public TimeSource
{
public:
    TestTimeSource(uint64_t base, TimeUnit unit, bool reliable=false);

    uint64_t now(TimeUnit unit);
    void arm(Timer *timer);
    void cancel(Timer *timer);

    void advance(uint64_t by, TimeUnit unit);

private:
    uint64_t time;
    std::list<Timer *> events;
    bool reliable;
};

#endif
