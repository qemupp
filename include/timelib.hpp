#ifndef TIMELIB_HPP
#define TIMELIB_HPP

#include "timer.hpp"
#include "test-time-source.hpp"

#include <time.h>

bool check_drift(TestTimeSource *ts, uint64_t frequency, TimeUnit unit,
		 DriftMode mode, int *intcnt);

time_t gettimestamp(void);

#endif
