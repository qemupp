#ifndef TIMER_HPP
#define TIMER_HPP

#include <functional>
#include <inttypes.h>

#include "marshal.hpp"

#define USEC_PER_SEC 1000000

enum DriftMode
{
    DRIFT_DROP,
    DRIFT_REINJECT_GRADUAL,
    DRIFT_REINJECT_FAST,
};

enum TimeUnit
{
    SEC = 0,
    MSEC,
    USEC,
    NSEC,
    PC_FREQ0,  /* 1.193182Mhz */
    PC_FREQ1,  /* 32.768 Khz */
};

class Timer
{
public:
    Timer(void);

    void set(std::function<void (void)> func);

    template <typename T>
    void set(T *obj, void (T::*method)(void)) {
        this->func = std::bind(method, obj);
    }

    void update(uint64_t next_deadline, TimeUnit unit);
    void cancel(void);

    uint64_t get_deadline(void);
    void fire(void);

    void pickle(Marshaller *m, const char *name);

private:
    uint64_t deadline;
    std::function<void (void)> func;
};

class TimeSource
{
public:
    static TimeSource *get_instance(void);

    virtual uint64_t now(TimeUnit unit) = 0;
    virtual void arm(Timer *timer) = 0;
    virtual void cancel(Timer *timer) = 0;
};

static inline uint64_t now(TimeUnit unit)
{
    return TimeSource::get_instance()->now(unit);
}

static inline void marshal(Marshaller *m, const char *name, DriftMode *mode)
{
    uint32_t val = (int)*mode;
    marshal(m, name, &val);
    *mode = (DriftMode)val;
}

uint64_t time_to_ns(uint64_t when, TimeUnit unit);
uint64_t ns_to_time(uint64_t when, TimeUnit unit);

void register_time_source(TimeSource *ts);

#endif
