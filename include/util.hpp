#ifndef UTIL_HPP
#define UTIL_HPP

#include <stdlib.h>
#include <stdint.h>
#include <exception>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

struct OutOfBounds : public std::exception
{
};

template <typename T, size_t N>
class Array
{
public:
    const T &at(size_t index) const {
        if (index < 0 || index >= N) {
            throw new OutOfBounds();
        }
        return this->data[index];
    }

    T &at(size_t index) {
        if (index < 0 || index >= N) {
            throw new OutOfBounds();
        }
        return this->data[index];
    }

    const T &operator[](size_t index) const {
        return this->at(index);
    }

    T &operator[](size_t index) {
        return this->at(index);
    }

    size_t size(void) const {
        return N;
    }

private:
    T data[N];
};

/* compute with 96 bit intermediate result: (a*b)/c */
static inline uint64_t muldiv64(uint64_t a, uint32_t b, uint32_t c)
{
    union {
        uint64_t ll;
        struct {
            /* FIXME byte order */
            uint32_t low, high;
        } l;
    } u, res;
    uint64_t rl, rh;

    u.ll = a;
    rl = (uint64_t)u.l.low * (uint64_t)b;
    rh = (uint64_t)u.l.high * (uint64_t)b;
    rh += (rl >> 32);
    res.l.high = rh / c;
    res.l.low = (((rh % c) << 32) + (rl & 0xffffffff)) / c;
    return res.ll;
}

template <typename T, typename K>
bool contains(const T &container, const K &key)
{
    if (container.find(key) != container.end()) {
        return true;
    }
    return false;
}

static inline int randint(int begin, int end)
{
    return begin + (random() % (end - begin));
}

#endif
