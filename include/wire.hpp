#ifndef WIRE_HPP
#define WIRE_HPP

#include "socket.hpp"
#include "pin.hpp"

class Wire
{
public:
    Wire(Socket<Pin> &socket) : socket(socket) {
        std::function<void (void)> fn;

        fn = std::bind(&Wire::on_plug, this);
        this->plugh = this->socket.connect(Socket<Pin>::PLUGGED, fn);
        fn = std::bind(&Wire::on_unplug, this);
        this->unplugh = this->socket.connect(Socket<Pin>::UNPLUGGED, fn);

        if (this->socket) {
            this->on_plug();
        }
    }

    ~Wire(void) {
#if 0
        // FIXME
        if (this->socket) {
            this->on_unplug();
        }
        this->socket.disconnect(this->plugh);
        this->socket.disconnect(this->unplugh);
#endif
    }

    Pin out;

private:
    void update_out(void) {
        this->out.set(this->socket->get());
    }

    void on_plug(void) {
        std::function<void (void)> fn;
        fn = std::bind(&Wire::update_out, this);
        this->risingh = this->socket->connect(Pin::RISING_EDGE, fn);
        this->fallingh = this->socket->connect(Pin::FALLING_EDGE, fn);
        this->update_out();
    }

    void on_unplug(void) {
        this->socket.disconnect(this->risingh);
        this->socket.disconnect(this->fallingh);
    }

    Socket<Pin> &socket;
    NotifierHandle plugh, unplugh;
    NotifierHandle risingh, fallingh;
};

#endif
