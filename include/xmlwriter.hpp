#ifndef XML_WRITER_HPP
#define XML_WRITER_HPP

#include "marshal.hpp"

#include <string>

class XMLWriter : public Marshaller
{
public:
    XMLWriter(bool schema_only=false);

    void marshal_int8(const char *name, int8_t *obj);
    void marshal_int16(const char *name, int16_t *obj);
    void marshal_int32(const char *name, int32_t *obj);
    void marshal_int64(const char *name, int64_t *obj);

    void marshal_uint8(const char *name, uint8_t *obj);
    void marshal_uint16(const char *name, uint16_t *obj);
    void marshal_uint32(const char *name, uint32_t *obj);
    void marshal_uint64(const char *name, uint64_t *obj);

    void marshal_bool(const char *name, bool *obj);

    void start_struct(const char *name, const char *type);
    void end_struct(void);

    void start_array(const char *name);
    void end_array(void);

    const std::string &as_string(void);

private:
    const char *indent(void);
    const char *name(const char *str);
    void emit(const char *fmt, ...);

    int indent_level;
    std::string indent_buf;
    std::string name_buf;
    std::string buffer;

    bool schema_only;
};

#endif
