#include "marshal.hpp"

void marshal(Marshaller *m, const char *name, int8_t *obj)
{
    m->marshal_int8(name, obj);
}

void marshal(Marshaller *m, const char *name, int16_t *obj)
{
    m->marshal_int16(name, obj);
}

void marshal(Marshaller *m, const char *name, int32_t *obj)
{
    m->marshal_int32(name, obj);
}

void marshal(Marshaller *m, const char *name, int64_t *obj)
{
    m->marshal_int64(name, obj);
}

void marshal(Marshaller *m, const char *name, uint8_t *obj)
{
    m->marshal_uint8(name, obj);
}

void marshal(Marshaller *m, const char *name, uint16_t *obj)
{
    m->marshal_uint16(name, obj);
}

void marshal(Marshaller *m, const char *name, uint32_t *obj)
{
    m->marshal_uint32(name, obj);
}

void marshal(Marshaller *m, const char *name, uint64_t *obj)
{
    m->marshal_uint64(name, obj);
}

void marshal(Marshaller *m, const char *name, bool *obj)
{
    m->marshal_bool(name, obj);
}
