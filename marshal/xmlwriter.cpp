#include "xmlwriter.hpp"

#include <stdarg.h>

XMLWriter::XMLWriter(bool schema_only) :
    indent_level(0), schema_only(schema_only)
{
}

void XMLWriter::marshal_int8(const char *name, int8_t *obj)
{
    if (this->schema_only) {
        this->emit("<int8%s/>", this->name(name));
    } else {
        this->emit("<int8%s>%u</int8>", this->name(name), *obj);
    }
        
}

void XMLWriter::marshal_int16(const char *name, int16_t *obj)
{
    if (this->schema_only) {
        this->emit("<int16%s/>", this->name(name));
    } else {
        this->emit("<int16%s>%u</int16>", this->name(name), *obj);
    }
}

void XMLWriter::marshal_int32(const char *name, int32_t *obj)
{
    if (this->schema_only) {
        this->emit("<int32%s/>", this->name(name));
    } else {
        this->emit("<int32%s>%u</int32>", this->name(name), *obj);
    }
}

void XMLWriter::marshal_int64(const char *name, int64_t *obj)
{
    if (this->schema_only) {
        this->emit("<int64%s/>", this->name(name));
    } else {
        this->emit("<int64%s>%lu</int64>", this->name(name), *obj);
    }
}

void XMLWriter::marshal_uint8(const char *name, uint8_t *obj)
{
    if (this->schema_only) {
        this->emit("<uint8%s/>", this->name(name));
    } else {
        this->emit("<uint8%s>%u</uint8>", this->name(name), *obj);
    }
}

void XMLWriter::marshal_uint16(const char *name, uint16_t *obj)
{
    if (this->schema_only) {
        this->emit("<uint16%s/>", this->name(name));
    } else {
        this->emit("<uint16%s>%u</uint16>", this->name(name), *obj);
    }
}

void XMLWriter::marshal_uint32(const char *name, uint32_t *obj)
{
    if (this->schema_only) {
        this->emit("<uint32%s/>", this->name(name));
    } else {
        this->emit("<uint32%s>%u</uint32>", this->name(name), *obj);
    }
}

void XMLWriter::marshal_uint64(const char *name, uint64_t *obj)
{
    if (this->schema_only) {
        this->emit("<uint64%s/>", this->name(name));
    } else {
        this->emit("<uint64%s>%lu</uint64>", this->name(name), *obj);
    }
}

void XMLWriter::marshal_bool(const char *name, bool *obj)
{
    if (this->schema_only) {
        this->emit("<bool%s/>", this->name(name));
    } else {
        this->emit("<bool%s>%s</bool>", this->name(name), *obj ? "true" : "false");
    }
}

void XMLWriter::start_struct(const char *name, const char *type)
{
    this->emit("<struct%s type=\"%s\">", this->name(name), type);
    this->indent_level += 4;
}

void XMLWriter::end_struct(void)
{
    this->indent_level -= 4;
    this->emit("</struct>");
}

void XMLWriter::start_array(const char *name)
{
    this->emit("<array%s>", this->name(name));
    this->indent_level += 4;
}

void XMLWriter::end_array(void)
{
    this->indent_level -= 4;
    this->emit("</array>");
}

const std::string &XMLWriter::as_string(void)
{
    return this->buffer;
}

const char *XMLWriter::indent(void)
{
    int i;

    this->indent_buf = "";

    for (i = 0; i < indent_level; i++) {
        this->indent_buf += " ";
    }

    return this->indent_buf.c_str();
}

const char *XMLWriter::name(const char *str)
{
    if (str) {
        this->name_buf = " name=\"";
        this->name_buf += str;
        this->name_buf += "\"";
    } else {
        this->name_buf = "";
    }

    return this->name_buf.c_str();
}

void XMLWriter::emit(const char *fmt, ...)
{
    va_list ap;
    char buf[1024];

    va_start(ap, fmt);
    vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);

    this->buffer += this->indent();
    this->buffer += buf;
    this->buffer += "\n";
}
