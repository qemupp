#include "i8042.hpp"

#include <stdio.h>

/*	Keyboard Controller Commands */
#define KBD_CCMD_READ_MODE	0x20	/* Read mode bits */
#define KBD_CCMD_WRITE_MODE	0x60	/* Write mode bits */
#define KBD_CCMD_GET_VERSION	0xA1	/* Get controller version */
#define KBD_CCMD_MOUSE_DISABLE	0xA7	/* Disable mouse interface */
#define KBD_CCMD_MOUSE_ENABLE	0xA8	/* Enable mouse interface */
#define KBD_CCMD_TEST_MOUSE	0xA9	/* Mouse interface test */
#define KBD_CCMD_SELF_TEST	0xAA	/* Controller self test */
#define KBD_CCMD_KBD_TEST	0xAB	/* Keyboard interface test */
#define KBD_CCMD_KBD_DISABLE	0xAD	/* Keyboard interface disable */
#define KBD_CCMD_KBD_ENABLE	0xAE	/* Keyboard interface enable */
#define KBD_CCMD_READ_INPORT    0xC0    /* read input port */
#define KBD_CCMD_READ_OUTPORT	0xD0    /* read output port */
#define KBD_CCMD_WRITE_OUTPORT	0xD1    /* write output port */
#define KBD_CCMD_WRITE_OBUF	0xD2
#define KBD_CCMD_WRITE_AUX_OBUF	0xD3    /* Write to output buffer as if
					   initiated by the auxiliary device */
#define KBD_CCMD_WRITE_MOUSE	0xD4	/* Write the following byte to the mouse */
#define KBD_CCMD_DISABLE_A20    0xDD    /* HP vectra only ? */
#define KBD_CCMD_ENABLE_A20     0xDF    /* HP vectra only ? */
#define KBD_CCMD_PULSE_BITS_3_0 0xF0    /* Pulse bits 3-0 of the output port P2. */
#define KBD_CCMD_RESET          0xFE    /* Pulse bit 0 of the output port P2 = CPU reset. */
#define KBD_CCMD_NO_OP          0xFF    /* Pulse no bits of the output port P2. */

/* Status Register Bits */
#define KBD_STAT_OBF 		0x01	/* Keyboard output buffer full */
#define KBD_STAT_IBF 		0x02	/* Keyboard input buffer full */
#define KBD_STAT_SELFTEST	0x04	/* Self test successful */
#define KBD_STAT_CMD		0x08	/* Last write was a command write (0=data) */
#define KBD_STAT_UNLOCKED	0x10	/* Zero if keyboard locked */
#define KBD_STAT_MOUSE_OBF	0x20	/* Mouse output buffer full */
#define KBD_STAT_GTO 		0x40	/* General receive/xmit timeout */
#define KBD_STAT_PERR 		0x80	/* Parity error */

/* Controller Mode Register Bits */
#define KBD_MODE_KBD_INT	0x01	/* Keyboard data generate IRQ1 */
#define KBD_MODE_MOUSE_INT	0x02	/* Mouse data generate IRQ12 */
#define KBD_MODE_SYS 		0x04	/* The system flag (?) */
#define KBD_MODE_NO_KEYLOCK	0x08	/* The keylock doesn't affect the keyboard if set */
#define KBD_MODE_DISABLE_KBD	0x10	/* Disable keyboard interface */
#define KBD_MODE_DISABLE_MOUSE	0x20	/* Disable mouse interface */
#define KBD_MODE_KCC 		0x40	/* Scan code conversion to PC format */
#define KBD_MODE_RFU		0x80

/* Output Port Bits */
#define KBD_OUT_RESET           0x01    /* 1=normal mode, 0=reset */
#define KBD_OUT_A20             0x02    /* x86 only */
#define KBD_OUT_OBF             0x10    /* Keyboard output buffer full */
#define KBD_OUT_MOUSE_OBF       0x20    /* Mouse output buffer full */

#define KBD_PENDING_KBD         1
#define KBD_PENDING_AUX         2

I8042::I8042(void)
{
    this->reset();
}

/* update irq and KBD_STAT_[MOUSE_]OBF */
/* XXX: not generating the irqs if KBD_MODE_DISABLE_KBD is set may be
   incorrect, but it avoids having to simulate exact delays */
void I8042::update_irq(void)
{
    int irq_kbd_level, irq_mouse_level;
    bool kbd_irq, aux_irq;

    irq_kbd_level = 0;
    irq_mouse_level = 0;
    this->status &= ~(KBD_STAT_OBF | KBD_STAT_MOUSE_OBF);
    this->outport &= ~(KBD_OUT_OBF | KBD_OUT_MOUSE_OBF);

    kbd_irq = this->kbd && this->kbd->can_read();
    aux_irq = this->aux && this->aux->can_read();

    if (kbd_irq || aux_irq) {
        this->status |= KBD_STAT_OBF;
        this->outport |= KBD_OUT_OBF;
        /* kbd data takes priority over aux data.  */
        if (!kbd_irq && aux_irq) {
            this->status |= KBD_STAT_MOUSE_OBF;
            this->outport |= KBD_OUT_MOUSE_OBF;
            if (this->mode & KBD_MODE_MOUSE_INT) {
                irq_mouse_level = 1;
            }
        } else {
            if ((this->mode & KBD_MODE_KBD_INT) &&
                !(this->mode & KBD_MODE_DISABLE_KBD)) {
                irq_kbd_level = 1;
            }
        }
    }
    this->irq_kbd.set(irq_kbd_level);
    this->irq_aux.set(irq_mouse_level);
}

void I8042::data_ready(PS2Device *device)
{
    this->update_irq();
}

void I8042::insert_kbd(PS2Keyboard *kbd)
{
    this->kbd = kbd;
    this->kbd->bus = this;
    this->add_subordinate(kbd);
}

void I8042::insert_aux(PS2Mouse *aux)
{
    this->aux = aux;
    this->aux->bus = this;
    this->add_subordinate(aux);
}

uint32_t I8042::read_status(void)
{
    return this->status;
}

void I8042::write_command(uint32_t val)
{
    /* Bits 3-0 of the output port P2 of the keyboard controller may be pulsed
     * low for approximately 6 micro seconds. Bits 3-0 of the KBD_CCMD_PULSE
     * command specify the output port bits to be pulsed.
     * 0: Bit should be pulsed. 1: Bit should not be modified.
     * The only useful version of this command is pulsing bit 0,
     * which does a CPU reset.
     */
    if((val & KBD_CCMD_PULSE_BITS_3_0) == KBD_CCMD_PULSE_BITS_3_0) {
        if(!(val & 1)) {
            val = KBD_CCMD_RESET;
        } else {
            val = KBD_CCMD_NO_OP;
        }
    }

    switch(val) {
    case KBD_CCMD_READ_MODE:
        if (this->kbd) {
            this->kbd->queue(this->mode);
        }
        break;
    case KBD_CCMD_WRITE_MODE:
    case KBD_CCMD_WRITE_OBUF:
    case KBD_CCMD_WRITE_AUX_OBUF:
    case KBD_CCMD_WRITE_MOUSE:
    case KBD_CCMD_WRITE_OUTPORT:
        this->write_cmd = val;
        break;
    case KBD_CCMD_MOUSE_DISABLE:
        this->mode |= KBD_MODE_DISABLE_MOUSE;
        break;
    case KBD_CCMD_MOUSE_ENABLE:
        this->mode &= ~KBD_MODE_DISABLE_MOUSE;
        break;
    case KBD_CCMD_TEST_MOUSE:
        if (this->kbd) {
            this->kbd->queue(0x00);
        }
        break;
    case KBD_CCMD_SELF_TEST:
        this->status |= KBD_STAT_SELFTEST;
        if (this->kbd) {
            this->kbd->queue(0x55);
        }
        break;
    case KBD_CCMD_KBD_TEST:
        if (this->kbd) {
            this->kbd->queue(0x00);
        }
        break;
    case KBD_CCMD_KBD_DISABLE:
        this->mode |= KBD_MODE_DISABLE_KBD;
        this->update_irq();
        break;
    case KBD_CCMD_KBD_ENABLE:
        this->mode &= ~KBD_MODE_DISABLE_KBD;
        this->update_irq();
        break;
    case KBD_CCMD_READ_INPORT:
        if (this->kbd) {
            this->kbd->queue(0x00);
        }
        break;
    case KBD_CCMD_READ_OUTPORT:
        if (this->kbd) {
            this->kbd->queue(this->outport);
        }
        break;
    case KBD_CCMD_ENABLE_A20:
        this->a20_line.raise();
        this->outport |= KBD_OUT_A20;
        break;
    case KBD_CCMD_DISABLE_A20:
        this->a20_line.lower();
        this->outport &= ~KBD_OUT_A20;
        break;
    case KBD_CCMD_RESET:
        this->reset_line.raise();
        this->reset_line.lower();
        break;
    case KBD_CCMD_NO_OP:
        /* ignore that */
        break;
    default:
        break;
    }
}

uint32_t I8042::read_data(void)
{
    uint32_t val = 0;

    if (this->aux && this->aux->can_read()) {
        val = this->aux->read();
    } else if (this->kbd) {
        val = this->kbd->read();
    }

    return val;
}

void I8042::write_data(uint32_t val)
{
    switch(this->write_cmd) {
    case 0:
        if (this->kbd) {
            this->kbd->write(val);
        }
        break;
    case KBD_CCMD_WRITE_MODE:
        this->mode = val;
        if (this->kbd) {
            this->kbd->set_translation((this->mode & KBD_MODE_KCC) != 0);
        }
        /* ??? */
        this->update_irq();
        break;
    case KBD_CCMD_WRITE_OBUF:
        if (this->kbd) {
            this->kbd->queue(val);
        }
        break;
    case KBD_CCMD_WRITE_AUX_OBUF:
        if (this->aux) {
            this->aux->queue(val);
        }
        break;
    case KBD_CCMD_WRITE_OUTPORT:
        this->write_scpa(val);
        break;
    case KBD_CCMD_WRITE_MOUSE:
        if (this->aux) {
            this->aux->write(val);
        }
        break;
    default:
        break;
    }
    this->write_cmd = 0;
}

void I8042::write_scpa(uint8_t val)
{
    this->outport = val;
    this->a20_line.set((val >> 1) & 1);
    if (!(val & 1)) {
        this->reset_line.raise();
        this->reset_line.lower();
    }
}

uint8_t I8042::read_scpa(void)
{
    /* don't let keyboard status pollute System Control Port A */
    return this->outport & 0x03;
}

void I8042::reset(void)
{
    this->mode = KBD_MODE_KBD_INT | KBD_MODE_MOUSE_INT;
    this->status = KBD_STAT_CMD | KBD_STAT_UNLOCKED;
    this->outport = KBD_OUT_RESET | KBD_OUT_A20;
    this->write_cmd = 0;
}

void I8042::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "I8042");
    marshal(m, "write_cmd", &this->write_cmd);
    marshal(m, "status", &this->status);
    marshal(m, "mode", &this->mode);
    marshal(m, "outport", &this->outport);
    marshal(m, "kbd", &this->kbd);
    marshal(m, "aux", &this->aux);
    marshal(m, "irq_kbd", &this->irq_kbd);
    marshal(m, "irq_aux", &this->irq_aux);
    marshal(m, "a20_line", &this->a20_line);
    marshal(m, "reset_line", &this->reset_line);
    m->end_struct();
}
