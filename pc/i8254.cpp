#include "i8254.hpp"
#include "util.hpp"

#include <stdio.h>

#define RW_STATE_LSB 1
#define RW_STATE_MSB 2
#define RW_STATE_WORD0 3
#define RW_STATE_WORD1 4

PITChannel::PITChannel(void) :
    drift_mode(DRIFT_DROP)
{
    this->irq_timer.set(this, &PITChannel::on_irq_timer);
    this->reset();
}

void PITChannel::write_back_command(uint8_t val)
{
    if (!(val & 0x20)) {
        this->latch_count();
    }
    if (!(val & 0x10) && !this->status_latched) {
        /* status latch */
        /* XXX: add BCD and null count */
        this->status =  (this->get_out1(now(PC_FREQ0)) << 7) |
            (this->rw_mode << 4) |
            (this->mode << 1) |
            this->bcd;
        this->status_latched = 1;
    }
}

void PITChannel::write_control(uint8_t val)
{
    uint8_t access = (val >> 4) & 3;

    if (access == 0) {
        this->latch_count();
    } else {
        this->rw_mode = access;
        this->read_state = access;
        this->write_state = access;

        this->mode = (val >> 1) & 7;
        this->bcd = val & 1;
        this->missed_ticks = 0;
        /* XXX: update irq timer ? */
    }
}

void PITChannel::write_counter(uint8_t val)
{
    switch(this->write_state) {
    default:
    case RW_STATE_LSB:
        this->load_count(val);
        break;
    case RW_STATE_MSB:
        this->load_count(val << 8);
        break;
    case RW_STATE_WORD0:
        this->write_latch = val;
        this->write_state = RW_STATE_WORD1;
        break;
    case RW_STATE_WORD1:
        this->load_count(this->write_latch | (val << 8));
        this->write_state = RW_STATE_WORD0;
        break;
    }
}

uint8_t PITChannel::read(void)
{
    uint8_t ret;

    if (this->status_latched) {
        this->status_latched = 0;
        ret = this->status;
    } else if (this->count_latched) {
        switch (this->count_latched) {
        default:
        case RW_STATE_LSB:
            ret = this->latched_count & 0xff;
            this->count_latched = 0;
            break;
        case RW_STATE_MSB:
            ret = this->latched_count >> 8;
            this->count_latched = 0;
            break;
        case RW_STATE_WORD0:
            ret = this->latched_count & 0xff;
            this->count_latched = RW_STATE_MSB;
            break;
        }
    } else {
        switch (this->read_state) {
        default:
        case RW_STATE_LSB:
            count = this->get_count();
            ret = count & 0xff;
            break;
        case RW_STATE_MSB:
            count = this->get_count();
            ret = (count >> 8) & 0xff;
            break;
        case RW_STATE_WORD0:
            count = this->get_count();
            ret = count & 0xff;
            this->read_state = RW_STATE_WORD1;
            break;
        case RW_STATE_WORD1:
            count = this->get_count();
            ret = (count >> 8) & 0xff;
            this->read_state = RW_STATE_WORD0;
            break;
        }
    }
    return ret;
}

void PITChannel::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "PITChannel");
    marshal(m, "irq", &this->irq);
    marshal(m, "count", &this->count);
    marshal(m, "latched_count", &this->latched_count);
    marshal(m, "count_latched", &this->count_latched);
    marshal(m, "status_latched", &this->status_latched);
    marshal(m, "status", &this->status);
    marshal(m, "read_state", &this->read_state);
    marshal(m, "write_state", &this->write_state);
    marshal(m, "write_latch", &this->write_latch);
    marshal(m, "rw_mode", &this->rw_mode);
    marshal(m, "mode", &this->mode);
    marshal(m, "bcd", &this->bcd);
    marshal(m, "count_load_time", &this->count_load_time);
    marshal(m, "next_transition_time", &this->next_transition_time);
    marshal(m, "irq_timer", &this->irq_timer);
    marshal(m, "drift_mode", &this->drift_mode);
    m->end_struct();
}

void PITChannel::reset(void)
{
    this->count_latched = 0;
    this->status_latched = 0;
    this->mode = 3;
    this->load_count(2);
}

int64_t PITChannel::get_next_transition_time(uint64_t current_time)
{
    uint64_t d, next_time, base;
    uint32_t period2;

    d = current_time - this->count_load_time;
    switch (this->mode) {
    default:
    case 0:
    case 1:
    case 4:
    case 5:
        if (d < this->count) {
            next_time = this->count_load_time + this->count;
        } else {
            return -1;
        }
        break;
    case 2:
        if (this->next_transition_time == -1) {
            next_time = this->count_load_time + this->count;
        } else {
            next_time = this->next_transition_time + this->count;
        }

        switch (this->drift_mode) {
        case DRIFT_DROP:
            if (next_time < current_time) {
                next_time = current_time + this->count - this->count_load_time;
                next_time = (next_time / this->count) * this->count;
                next_time += this->count_load_time;
            }
            break;
        case DRIFT_REINJECT_GRADUAL:
            if (next_time < current_time) {
                uint64_t missed_time = current_time - next_time;
                uint64_t ticks = (missed_time / this->count);
                    
                /* I don't understand this +1 .  It could be a rounding
                 * issue.  I know that we need to see 4 missed ticks when
                 * not in gradual mode
                 */
                this->missed_ticks += 2 * (ticks + 1);
                next_time = next_time + ticks * this->count + this->count;
            }
            if (this->missed_ticks) {
                next_time -= this->count / 2;
                this->missed_ticks--;
            }
            break;
        case DRIFT_REINJECT_FAST:
            /* nothing needed */
            break;
        }
        break;
    case 3:
        base = (d / this->count) * this->count;
        period2 = ((this->count + 1) >> 1);
        if ((d - base) < period2) {
            next_time = base + period2;
        } else {
            next_time = base + this->count;
        }
        next_time += this->count_load_time;
        break;
    }

    return next_time;
}

void PITChannel::update_irq(uint64_t current_time)
{
    int irq_level;

    /* This returns the precise status of the OUT pin */
    irq_level = this->get_out1(current_time);

    switch (this->mode) {
    case 0:
    case 1:
    case 3:
    case 5:
        this->irq.set(irq_level);
        break;
    case 2:
    case 4:
        /* Mode 2 is a periodic timer that fires for exactly 1 bus cycle
         * when the counter hits 1.  We would have to do a cycle accurate
         * simulation to model this perfectly.
         *
         * Instead, we set the OUT line status based on whether we're at
         * the precise bus cycle where the periodic timer fires but we
         * also check to see whether we've exceeded the current periodic
         * deadline.  If so, we fire the timer.
         */
        if (!irq_level &&
            (uint64_t)this->next_transition_time <= current_time) {
            irq_level = 1;
        }
        /*
         * Mode 4 is a one shot timer that fires when count hits 1 and then
         * holds OUT high for 1 additional clock cycle.
         *
         * We can't pulse the OUT line for exactly one bus cycle without
         * registering a very short timeout which we probably can't reach
         * anyway.  Instead, pulse the line immediately.  This is enough
         * for QEMU and KVM to inject the interrupt into the guest since both
         * do edge-triggered injection.
         */
        if (irq_level) {
            this->irq.set(1);
        }
        this->irq.set(0);
        break;
    }
}

void PITChannel::irq_timer_update(uint64_t current_time)
{
    int64_t expire_time;

    expire_time = this->get_next_transition_time(current_time);
    this->update_irq(current_time);
    this->next_transition_time = expire_time;
    if (expire_time != -1) {
        this->irq_timer.update(expire_time, PC_FREQ0);
    } else {
        this->irq_timer.cancel();
    }
}

void PITChannel::on_irq_timer(void)
{
    this->irq_timer_update(now(PC_FREQ0));
}

/* get pit output bit */
int PITChannel::get_out1(int64_t current_time)
{
    uint64_t d;
    int out;

    d = current_time - this->count_load_time;
    switch (this->mode) {
    default:
    case 0:
        out = (d >= this->count);
        break;
    case 1:
        out = (d < this->count);
        break;
    case 2:
        if ((d % this->count) == 0 && d != 0) {
            out = 1;
        } else {
            out = 0;
        }
        break;
    case 3:
        out = (d % this->count) < ((this->count + 1) >> 1);
        break;
    case 4:
    case 5:
        out = (d == this->count);
        break;
    }
    return out;
}

void PITChannel::load_count(int val)
{
    if (val == 0) {
        val = 0x10000;
    }
    this->count_load_time = now(PC_FREQ0);
    this->count = val;
    this->irq_timer_update(this->count_load_time);
}

/* if already latched, do not latch again */
void PITChannel::latch_count(void)
{
    if (!this->count_latched) {
        this->latched_count = this->get_count();
        this->count_latched = this->rw_mode;
    }
}

uint16_t PITChannel::get_count(void)
{
    uint64_t d;
    uint16_t counter;

    d = now(PC_FREQ0) - this->count_load_time;
    switch (this->mode) {
    case 0:
    case 1:
    case 4:
    case 5:
        counter = (this->count - d) & 0xffff;
        break;
    case 3:
        /* XXX: may be incorrect for odd counts */
        counter = this->count - ((2 * d) % this->count);
        break;
    default:
        counter = this->count - (d % this->count);
        break;
    }
    return counter;
}

void PITChannel::set_drift_mode(DriftMode mode)
{
    this->drift_mode = mode;
    this->missed_ticks = 0;
}

PIT::PIT(DriftMode mode)
{
    for (int i = 0; i < 3; i++) {
        this->add_subordinate(&this->channels[i]);
        this->channels[i].set_drift_mode(mode);
    }
}

void PIT::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "PIT");
    marshal(m, "channels", &this->channels);
    m->end_struct();
}

void PIT::write(uint8_t addr, uint8_t val)
{
    uint8_t channel;
    PITChannel *s;

    addr &= 3;
    if (addr == 3) {
        channel = val >> 6;
        if (channel == 3) {
            /* read back command */
            for (channel = 0; channel < 3; channel++) {
                s = &this->channels[channel];
                if (val & (2 << channel)) {
                    s->write_back_command(val);
                }
            }
        } else {
            s = &this->channels[channel];
            s->write_control(val);
        }
    } else {
        s = &this->channels[addr];
        s->write_counter(val);
    }
}

uint8_t PIT::read(uint8_t addr)
{
    addr &= 3;
    if (addr == 0x03) {
        return 0xFF;
    }

    return this->channels[addr].read();
}

Pin &PIT::get_irq(void)
{
    return this->channels[0].irq;
}

#if 0
/* public interfaces that we don't yet expose */
/* need to figure out which of these make sense */

int pit_get_out(PITState *pit, int channel, int64_t current_time);

int pit_get_initial_count(PITState *pit, int channel);

int pit_get_mode(PITState *pit, int channel);

void hpet_pit_disable(void);

void hpet_pit_enable(void);

#endif
