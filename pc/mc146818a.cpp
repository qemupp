#include "mc146818a.hpp"
#include "mc146818a_def.hpp"
#include "util.hpp"

#include <string.h>
#include <stdio.h>

/* month is between 0 and 11. */
static int get_days_in_month(int month, int year)
{
    static const int days_tab[12] = {
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
    int d;
    if ((unsigned )month >= 12) {
        return 31;
    }
    d = days_tab[month];
    if (month == 1) {
        if ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0)) {
            d++;
        }
    }
    return d;
}

/* update 'tm' to the next second */
static void next_second(struct tm *tm)
{
    int days_in_month;

    tm->tm_sec++;
    if ((unsigned)tm->tm_sec >= 60) {
        tm->tm_sec = 0;
        tm->tm_min++;
        if ((unsigned)tm->tm_min >= 60) {
            tm->tm_min = 0;
            tm->tm_hour++;
            if ((unsigned)tm->tm_hour >= 24) {
                tm->tm_hour = 0;
                /* next day */
                tm->tm_wday++;
                if ((unsigned)tm->tm_wday >= 7)
                    tm->tm_wday = 0;
                days_in_month = get_days_in_month(tm->tm_mon,
                                                  tm->tm_year + 1900);
                tm->tm_mday++;
                if (tm->tm_mday < 1) {
                    tm->tm_mday = 1;
                } else if (tm->tm_mday > days_in_month) {
                    tm->tm_mday = 1;
                    tm->tm_mon++;
                    if (tm->tm_mon >= 12) {
                        tm->tm_mon = 0;
                        tm->tm_year++;
                    }
                }
            }
        }
    }
}

RTC::RTC(int32_t base_year, struct tm *tm, DriftMode mode) :
    base_year(base_year), current_tm(*tm), drift_mode(mode)
{
    uint8_t val;

    for (size_t i = 0; i < this->cmos_data.size(); i++) {
        this->cmos_data[i] = 0;
    }

    this->periodic_timer.set(this, &RTC::on_periodic);
    this->second_timer.set(this, &RTC::on_second);
    this->second_timer2.set(this, &RTC::on_second2);

    val = this->to_bcd((tm->tm_year / 100) + 19);

    this->cmos_data[RTC_REG_A] = 0x26;
    this->cmos_data[RTC_REG_B] = 0x02;
    this->cmos_data[RTC_REG_C] = 0x00;
    this->cmos_data[RTC_REG_D] = 0x80;
    this->cmos_data[REG_IBM_CENTURY_BYTE] = val;
    this->cmos_data[REG_IBM_PS2_CENTURY_BYTE] = val;

    this->copy_date();
    this->next_second_time = now(SEC);
    next_second(&this->current_tm);
    this->second_timer2.update(this->next_second_time, SEC);
    this->reset();
}

void RTC::reset(void)
{
    this->cmos_data[RTC_REG_B] &= ~(REG_B_PIE | REG_B_AIE | REG_B_SQWE);
    this->cmos_data[RTC_REG_C] &= ~(REG_C_UF | REG_C_IRQF | REG_C_PF | REG_C_AF);
    this->irq.lower();
#if 1
    /* FIXME */
    this->periodic_timer.cancel();
    this->cmos_data[RTC_REG_A] = 0x26;
    this->cmos_data[RTC_REG_B] = 0x02;
#endif
}

uint8_t RTC::cmos_read(uint8_t index)
{
    uint8_t ret;

    switch (index) {
    case RTC_SECONDS:
    case RTC_MINUTES:
    case RTC_HOURS:
    case RTC_DAY_OF_WEEK:
    case RTC_DAY_OF_MONTH:
    case RTC_MONTH:
    case RTC_YEAR:
        ret = this->cmos_data[index];
        break;
    case RTC_REG_A:
        ret = this->cmos_data[index];
        break;
    case RTC_REG_C:
        ret = this->cmos_data[index];
        this->irq.lower();
        this->cmos_data[RTC_REG_C] = 0x00;
        break;
    default:
        ret = this->cmos_data[index];
        break;
    }
    
    return ret;
}

void RTC::cmos_write(uint8_t index, uint8_t data)
{
    switch (index) {
    case RTC_SECONDS_ALARM:
    case RTC_MINUTES_ALARM:
    case RTC_HOURS_ALARM:
        this->cmos_data[index] = data;
        break;
    case RTC_SECONDS:
    case RTC_MINUTES:
    case RTC_HOURS:
    case RTC_DAY_OF_WEEK:
    case RTC_DAY_OF_MONTH:
    case RTC_MONTH:
    case RTC_YEAR:
        this->cmos_data[index] = data;
        /* if in set mode, do not update the time */
        if (!(this->cmos_data[RTC_REG_B] & REG_B_SET)) {
            this->set_time();
        }
        break;
    case RTC_REG_A:
        /* UIP bit is read only */
        this->cmos_data[RTC_REG_A] &= REG_A_UIP;
        this->cmos_data[RTC_REG_A] |= (data & ~REG_A_UIP);
	this->next_periodic_time = -1;
        this->missed_ticks = 0;
        this->update_timer(now(PC_FREQ1));
        break;
    case RTC_REG_B:
        if (data & REG_B_SET) {
            /* set mode: reset UIP mode */
            this->cmos_data[RTC_REG_A] &= ~REG_A_UIP;
            data &= ~REG_B_UIE;
        } else {
            /* if disabling set mode, update the time */
            if (this->cmos_data[RTC_REG_B] & REG_B_SET) {
                this->set_time();
            }
        }
        this->cmos_data[RTC_REG_B] = data;
	this->next_periodic_time = -1;
        this->missed_ticks = 0;
        this->update_timer(now(PC_FREQ1));
        break;
    case RTC_REG_C:
    case RTC_REG_D:
        /* cannot write to them */
        break;
    default:
        this->cmos_data[index] = data;
        break;
    }
}

int RTC::from_bcd(int a)
{
    if (this->cmos_data[RTC_REG_B] & REG_B_DM) {
        return a;
    } else {
        return ((a >> 4) * 10) + (a & 0x0f);
    }
}

int RTC::to_bcd(int a)
{
    if (this->cmos_data[RTC_REG_B] & REG_B_DM) {
        return a;
    } else {
        return ((a / 10) << 4) | (a % 10);
    }
}

void RTC::update_timer(int64_t current_time)
{
    int period_code;
    uint64_t period;

    period_code = this->cmos_data[RTC_REG_A] & 0x0f;
    if ((period_code == 0) ||
        !(this->cmos_data[RTC_REG_B] & REG_B_PIE)) {
        this->periodic_timer.cancel();
        return;
    }

    if (period_code <= 2) {
        period_code += 7;
    }
    /* period in 32 Khz cycles */
    period = 1 << (period_code - 1);

    if (this->next_periodic_time == -1) {
        this->next_periodic_time = current_time;
    }
    this->next_periodic_time += period;

    switch (this->drift_mode) {
    case DRIFT_DROP:
        /* Our next timer fires backwards in time, so drop the missed interrupts
         * and advance the timer to the next logical period boundary.
         */
        if (this->next_periodic_time < current_time) {
            uint64_t missed_time;

            /* Compute the amount of time that we missed rounded to the period
             * boundary.  This is the time we ignore and drop interrupts for.
             * The rounding step is important to make the timer stable with
             * respect its period boundaries.
             */
            missed_time = current_time - this->next_periodic_time;
            missed_time &= ~(period - 1);
            this->next_periodic_time += missed_time + period;
        }
        break;
    case DRIFT_REINJECT_GRADUAL:
        if (this->next_periodic_time < current_time) {
                uint64_t missed_time = current_time - this->next_periodic_time;
                uint64_t ticks = (missed_time / period);
                    
                /* I don't understand this +1 .  It could be a rounding
                 * issue.  I know that we need to see 4 missed ticks when
                 * not in gradual mode
                 */
                this->missed_ticks += 2 * (ticks + 1);
                this->next_periodic_time += ticks * period + period;
            }
            if (this->missed_ticks) {
                this->next_periodic_time -= period / 2;
                this->missed_ticks--;
            }
            break;
    case DRIFT_REINJECT_FAST:
        /* nothing to do */
        break;
    }

    this->periodic_timer.update(this->next_periodic_time, PC_FREQ1);
}

void RTC::set_time(void)
{
    struct tm *tm = &this->current_tm;

    tm->tm_sec = this->from_bcd(this->cmos_data[RTC_SECONDS]);
    tm->tm_min = this->from_bcd(this->cmos_data[RTC_MINUTES]);
    tm->tm_hour = this->from_bcd(this->cmos_data[RTC_HOURS] & 0x7f);
    if (!(this->cmos_data[RTC_REG_B] & 0x02) &&
        (this->cmos_data[RTC_HOURS] & 0x80)) {
        tm->tm_hour += 12;
    }
    tm->tm_wday = this->from_bcd(this->cmos_data[RTC_DAY_OF_WEEK]) - 1;
    tm->tm_mday = this->from_bcd(this->cmos_data[RTC_DAY_OF_MONTH]);
    tm->tm_mon = this->from_bcd(this->cmos_data[RTC_MONTH]) - 1;
    tm->tm_year = this->from_bcd(this->cmos_data[RTC_YEAR]) +
        this->base_year - 1900;
}

void RTC::copy_date(void)
{
    const struct tm *tm = &this->current_tm;
    int year;

    this->cmos_data[RTC_SECONDS] = this->to_bcd(tm->tm_sec);
    this->cmos_data[RTC_MINUTES] = this->to_bcd(tm->tm_min);
    if (this->cmos_data[RTC_REG_B] & 0x02) {
        /* 24 hour format */
        this->cmos_data[RTC_HOURS] = this->to_bcd(tm->tm_hour);
    } else {
        /* 12 hour format */
        this->cmos_data[RTC_HOURS] = this->to_bcd(tm->tm_hour % 12);
        if (tm->tm_hour >= 12)
            this->cmos_data[RTC_HOURS] |= 0x80;
    }
    this->cmos_data[RTC_DAY_OF_WEEK] = this->to_bcd(tm->tm_wday + 1);
    this->cmos_data[RTC_DAY_OF_MONTH] = this->to_bcd(tm->tm_mday);
    this->cmos_data[RTC_MONTH] = this->to_bcd(tm->tm_mon + 1);
    year = (tm->tm_year - this->base_year) % 100;
    if (year < 0) {
        year += 100;
    }
    this->cmos_data[RTC_YEAR] = this->to_bcd(year);
}

void RTC::on_periodic(void)
{
    this->update_timer(now(PC_FREQ1));
    if (this->cmos_data[RTC_REG_B] & REG_B_PIE) {
        this->cmos_data[RTC_REG_C] |= 0xc0;
        this->irq.raise();
    }
}

void RTC::on_second(void)
{
    /* if the oscillator is not in normal operation, we do not update */
    if ((this->cmos_data[RTC_REG_A] & 0x70) != 0x20) {
        this->next_second_time += 1;
        this->second_timer.update(this->next_second_time, SEC);
    } else {
        uint64_t next_deadline;

        next_second(&this->current_tm);

        if (!(this->cmos_data[RTC_REG_B] & REG_B_SET)) {
            /* update in progress bit */
            this->cmos_data[RTC_REG_A] |= REG_A_UIP;
        }

        next_deadline = time_to_ns(this->next_second_time, SEC);
        next_deadline += time_to_ns(244, USEC); 
        this->second_timer2.update(next_deadline, NSEC);
    }
}

void RTC::on_second2(void)
{
    if (!(this->cmos_data[RTC_REG_B] & REG_B_SET)) {
        this->copy_date();
    }

    /* check alarm */
    if (this->cmos_data[RTC_REG_B] & REG_B_AIE) {
        if (((this->cmos_data[RTC_SECONDS_ALARM] & 0xc0) == 0xc0 ||
             this->from_bcd(this->cmos_data[RTC_SECONDS_ALARM]) == this->current_tm.tm_sec) &&
            ((this->cmos_data[RTC_MINUTES_ALARM] & 0xc0) == 0xc0 ||
             this->from_bcd(this->cmos_data[RTC_MINUTES_ALARM]) == this->current_tm.tm_min) &&
            ((this->cmos_data[RTC_HOURS_ALARM] & 0xc0) == 0xc0 ||
             this->from_bcd(this->cmos_data[RTC_HOURS_ALARM]) == this->current_tm.tm_hour)) {

            this->cmos_data[RTC_REG_C] |= 0xa0;
            this->irq.raise();
        }
    }

    /* update ended interrupt */
    this->cmos_data[RTC_REG_C] |= REG_C_UF;
    if (this->cmos_data[RTC_REG_B] & REG_B_UIE) {
        this->cmos_data[RTC_REG_C] |= REG_C_IRQF;
        this->irq.raise();
    }

    /* clear update in progress bit */
    this->cmos_data[RTC_REG_A] &= ~REG_A_UIP;

    this->next_second_time += 1;
    this->second_timer.update(this->next_second_time, SEC);
}

uint8_t RTC::read(uint8_t addr)
{
    if (addr == 0) {
        return 0xFF;
    }
    return this->cmos_read(this->cmos_index);
}

void RTC::write(uint8_t addr, uint8_t data)
{
    if (addr == 0) {
        this->cmos_index = data & 0x7F;
    } else {
        this->cmos_write(this->cmos_index, data);
    }
}

void RTC::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "RTC");
    marshal(m, "irq", &this->irq);
    marshal(m, "base_year", &this->base_year);
    marshal(m, "cmos_data", &this->cmos_data);
    marshal(m, "current_tm", &this->current_tm);
    marshal(m, "next_periodic_time", &this->next_periodic_time);
    marshal(m, "next_second_time", &this->next_periodic_time);
    marshal(m, "periodic_timer", &this->periodic_timer);
    marshal(m, "second_timer", &this->second_timer);
    marshal(m, "second_timer2", &this->second_timer2);
    marshal(m, "drift_mode", &this->drift_mode);
    marshal(m, "missed_ticks", &this->missed_ticks);
    marshal(m, "cmos_index", &this->cmos_index);
    m->end_struct();
}

/* FIXME This doesn't belong here */
void marshal(Marshaller *m, const char *name, struct tm *obj)
{
    m->start_struct(name, "tm");
    marshal(m, "tm_sec", &obj->tm_sec);
    marshal(m, "tm_min", &obj->tm_min);
    marshal(m, "tm_hour", &obj->tm_hour);
    marshal(m, "tm_mday", &obj->tm_mday);
    marshal(m, "tm_mon", &obj->tm_mon);
    marshal(m, "tm_year", &obj->tm_year);
    marshal(m, "tm_wday", &obj->tm_wday);
    marshal(m, "tm_yday", &obj->tm_yday);
    marshal(m, "tm_isdst", &obj->tm_isdst);
    m->end_struct();
}
