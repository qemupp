#include "southbridge.hpp"

SouthBridge::SouthBridge(int32_t base_year, struct tm *tm, DriftMode mode) :
    rtc(base_year, tm, mode), pit(mode)
{
}

void SouthBridge::write_port(uint16_t addr, uint32_t val)
{
    switch (addr) {
    case 0x40 ... 0x44:
        this->pit.write(addr - 0x40, val);
        break;
    case 0x60:
        this->i8042.write_data(val);
        break;
    case 0x64:
        this->i8042.write_command(val);
        break;
    case 0x70 ... 0x71:
        this->rtc.write(addr - 0x70, val);
        break;
    case 0x92:
        this->i8042.write_scpa(val);
        break;
    }
}

uint32_t SouthBridge::read_port(uint16_t addr)
{
    switch (addr) {
    case 0x40 ... 0x44:
        return this->pit.read(addr - 0x40);
    case 0x60:
        return this->i8042.read_data();
    case 0x64:
        return this->i8042.read_status();
    case 0x70 ... 0x71:
        return this->rtc.read(addr - 0x70);
    case 0x92:
        return this->i8042.read_scpa();
    }

    return 0xFFFFFFFF;
}

/* FIXME need to think carefully through reset and realize */
void SouthBridge::reset(void)
{
    this->i8042.reset();
    this->rtc.reset();
    this->pit.reset();
}

void SouthBridge::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "SouthBridge");
    marshal(m, "i8042", &this->i8042);
    marshal(m, "rtc", &this->rtc);
    marshal(m, "pit", &this->pit);
    m->end_struct();
}
