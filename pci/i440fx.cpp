/**
 * The i440fx is the motherboard chipset that was popular in the Pentium Pro
 * era.  At this point in time, the chipset included both the Northbridge
 * (Memory Controller) and Southbridge (IO devices) functionality.
 *
 * The i440fx has a PCI 2.1 compatible bus and a PCI-to-ISA bridge.  All port
 * I/O and main memory access goes through the i440fx with the exception of
 * access to the local APIC which is part of the processor.
 *
 * The main interface to the i440fx is the PCI bus.  The PCI and Memory
 * Controller (PMC) can be accessed as device 0:0.0 PCI config space.  It's
 * a little strange because the PMC is not really a PCI device but duplicates
 * some of the PCI device state.
 *
 * The i440fx also includes a PIIX3 which provides an ISA bus and a bunch of
 * the platform devices.
 *
 * This file implements the PCI Host Controller and Memory Controller
 * functionality (PMC).
 */

#include "piix3.hpp"

static bool addr_in_range(uint64_t addr, unsigned len,
                          uint64_t start, uint64_t end)
{
    return (addr >= start && (addr + len) < end);
}

struct MemoryRegion
{
    uint64_t start;
    uint64_t end;
    bool writeonly;
};

class I440FX : public Device
{
public:
    /**
     * Port I/O entry points.  It's expected that all PIO is sent through this
     * interface.
     */
    void pio_rw(uint16_t addr, unsigned len, uint32_t *data, bool read);

    /**
     * Memory entry points.  See mem_intercepts for usage.
     */
    void mem_rw(uint64_t addr, unsigned len, uint64_t *data, bool read);

    /**
     * Memory intercept API.  For performance reasons, instead of requiring all
     * memory accesses go through mem_write/mem_read, we provide a list of
     * regions that we care to go through our interface.  Any access to
     * something not in this area can be handled outside of the PMC.
     */
    const std::vector<MemoryRegion> &get_mem_intercepts(void) const;

    void set_mem_change_notifier(std::function<void (void)> fn);

private:
    /**
     * I/O directly to RAM
     */
    void ram_rw(uint64_t addr, unsigned len, uint64_t *data, bool read);

    /**
     * I/O to a ROM device
     */
    void rom_rw(uint64_t addr, unsigned len, uint64_t *data, bool read);

    /**
     * I/O to a PCI device.  At least, this is I/O that's forwarded to the PCI
     * bus.  It may or may not be handled by an actual device.
     *
     * It's not clear if we should fold rom_rw into pci_rw.  ISA roms probably
     * get accessed over the PCI bus but I doubt that the BIOS areas are
     * accessed through PCI.
     */
    void pci_rw(uint64_t addr, unsigned len, uint64_t *data, bool read);

    void confdata_rw(unsigned len, uint32_t *data, bool read);

    void pmc_rw(uint8_t regnum, unsigned len, uint32_t *data, bool read);

    void pci_pio_rw(uint16_t addr, unsigned len, uint32_t *data, bool read);

    bool addr_in_pci_range(uint64_t addr, unsigned len);

    PCIDevice *find_dev(uint8_t bus, uint8_t devfn);

    void add_mem_intercept(uint64_t addr, uint64_t end, bool writeonly);
    void remove_mem_intercept(uint64_t addr, uint64_t end);

private:
    std::vector<MemoryRegion> mem_intercepts;
    std::function<void (void)> fn;

    Plug<ROM> bios;

    PIIX3 piix3;

    Array<Plug<PCIDevce>, 32> slots; /* slot 0 is reserved by the PMC */

    uint32_t confadd;

    uint16_t vendor_id;
    uint16_t device_id;
    uint16_t command;
    uint16_t status;
    uint8_t rid;
    uint8_t prog_if;
    uint8_t subclass_code;
    uint8_t class_code;
    uint8_t mlt;
    uint8_t headt;
    uint8_t bist;
    uint16_t pmccfg;
    uint8_t deturbo;
    uint8_t dbc;
    uint8_t axc;
    uint16_t dramr;
    uint8_t dramc;
    uint8_t dramt;
    Array<uint8_t, 7> pam;
    Array<uint8_t, 8> drb;
    uint8_t fdhc;
    uint8_t mtt;
    uint8_t clt;
    uint8_t smram;
    uint8_t errcmd;
    uint8_t errsts;
    uint8_t trc;
};

I440FX::I440FX(void) :
    vendor_id(PCI_VENDOR_ID_INTEL), device_id(PCI_DEVICE_ID_INTEL_82441),
    rid(0x02), class_code(0x00), subclass_code(0x06), prog_if(0x00);
{
    this->slots[0].plug(&this->piix3);
}

void I440FX::add_mem_intercept(uint64_t addr, uint64_t end, bool writeonly)
{
    std::vector<MemoryRegion>::iterator i;
    MemRegion region = { addr, end, writeonly };

    this->remove_mem_intercept(addr, end);

    for (i = this->mem_intercepts.begin();
         i != this->mem_intercepts.end(); ++i) {
        if (end >= i->begin) {
            break;
        }
    }
    this->mem_intercepts.insert(i, region);
}

void I440FX::remove_mem_intercept(uint64_t addr, uint64_t end)
{
    /* FIXME */
}

void I440FX::update_ram_shadowing(void)
{
    /* The PAM registers make it possible to independently redirect reads and
     * writes in the BIOS ROM area to main memory.  The idea is to allow for
     * RAM shadowing which allows read-access for ROMs to come from main memory
     * whereas writes will continue to go to ROMs.  This can dramatically
     * improve BIOS execution speed on bare metal because ROM access is
     * generally very slow.
     *
     * Each PAM register is split into two nibbles.  The nibbles then have two
     * reserved bits and then a read-enable and write-enabled bit.  Each nibble
     * corresponds to 16k of ROM space except for PAM[0] which refers to 32k.
     *
     * The main objective of this function is to update the mem_intercept table
     * as the PAM registers change.
     */
    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 2; j++) {
            uint32_t start, end;

            if (i == 0 && j == 0) {
                /* Reserved */
                continue;
            }

            if (i == 0 && j == 1) {
                start = 0xF0000;
                end = 0xFFFFF;
            } else {
                start = 0xC0000 + (((i * 2) + j) * 0x4000);
                end = start + 0x3FFF;
            }

            switch ((this->pam[i] >> (i * 4)) & 0x3) {
            case 0: /* WE=0, RE=0 - accessed as normal ROM */
                /* Remove (start..end) from intercept list */
                this->remove_mem_intercept(start, end);
                break;
            case 1: /* WE=0, RE=1 - write to ROM, read from RAM */
                this->add_mem_intercept(start, end, true);
                break;
            case 2: /* WE=1, RE=0 - write to RAM, read from ROM */
                this->add_mem_intercept(start, end, false);
                break;
            case 3: /* WE=1, RE=1 - accessed as normal RAM */
                this->add_mem_intercept(start, end);
                break;
            }
        }
    }
}

void I440FX::pci_rw(uint64_t addr, unsigned len, uint64_t *data, bool read)
{
    /* walk through each PCI device and see what they are interested in */
}

void I440FX::pci_pio_rw(uint16_t addr, unsigned len, uint32_t *data, bool read)
{
    /* Search for a PCI device that claims this I/O address.  Use subtractive
     * decoding to forward unclaimed I/Os to the PIIX3's PCI-to-ISA bridge.
     */
}

bool I440FX::addr_in_pci_range(uint64_t addr, unsigned len)
{
}

PCIDevice *I440FX::find_bus(uint8_t bus)
{
    if (bus == 0) {
        return dynamic_cast<PCIBus>(this);
    }

    return PCIBus::find_bus(bus);
}

/* Interaction with the PCI host and Memory Controller
 * It looks a lot like a PCI device but it's really the PCI host.
 */
void I440FX::pmc_rw(uint8_t regnum, unsigned len, uint32_t *data, bool read)
{
    RegisterIO reg(regnum, len, data, read);

    reg.io(&this->vendor_id, 0x00, READONLY);
    reg.io(&this->device_id, 0x02, READONLY);
    reg.io(&this->command, 0x04, READWRITE, 0x029F);
    reg.io(&this->status,  0x06, WRITECLEAR, 0x380);
    reg.io(&this->rid, 0x08, READONLY);
    reg.io(&this->prog_if, 0x09, READONLY);
    reg.io(&this->subclass_code, 0x0A, READONLY);
    reg.io(&this->class_code, 0x0B, READONLY);
    reg.io(&this->mlt, 0x0D);
    reg.io(&this->headt, 0x0E);
    reg.io(&this->bist, 0x0F);

    reg.io(&this->pmccfg, 0x50, READWRITE, 0x4304);
    reg.io(&this->deturbo, 0x52);
    reg.io(&this->dbc, 0x53);
    reg.io(&this->axc, 0x54);
    reg.io(&this->dramr, 0x55);
    reg.io(&this->dramc, 0x57);
    reg.io(&this->dramt, 0x58);
    if (reg.io(&this->pam, 0x59)) {
        this->update_ram_shadowing();
    }
    reg.io(&this->drb, 0x60);
    reg.io(&this->fdhc, 0x68);
    reg.io(&this->mtt, 0x70);
    reg.io(&this->clt, 0x71);
    reg.io(&this->smram, 0x72);
    reg.io(&this->errcmd, 0x90);
    reg.io(&this->errsts, 0x91, WRITECLEAR);
    reg.io(&this->trc, 0x93, WRITECLEAR);
}

void I440FX::confdata_rw(unsigned len, uint32_t *data, bool read)
{
    uint8_t bus = (data >> 16) & 0xFF;
    uint8_t devfn = (data >> 8) & 0xFF;
    uint8_t regnum = data & 0xFE;

    if (bus == 0 && devfn == 0) {
        this->pmc_rw(regnum, len, data, read);
    } else {
        PCIDevice *dev = this->find_dev(bus, devfn);

        /* PCI spec suggests returning invalid config reads as all 1s in order
         * to report an invalid device/vendor id for empty slots.
         */
        if (read) {
            *data = 0xFFFFFFFF;
        }
        if (dev) {
            dev->config_rw(regnum, len, data, read);
        }
    }
    return data;
}

/**
 * Memory entry points.  The PMC is responsible for dealing with all memory
 * operations.
 *
 * The normal x86 memory layout looks like:
 *
 * 0x00000 .. 0xA0000      DOS Memory Area       RAM
 * 0xA0000 .. 0xC0000      Video Memory          Device Memory
 * 0xC0000 .. 0xE0000      ISA Extension ROM     ROM
 * 0xE0000 .. 0xF0000      BIOS Extension ROM    ROM
 * 0xF0000 .. 0x100000     BIOS Area             ROM
 *
 * The DOS Memory Area is just normal RAM.  Some areas have special common uses
 * in real mode.
 *
 * The Video Memory area is used by the Video BIOS and VGA adapter.  It maps the
 * VGA and CGA framebuffers and other registers.
 *
 * The ISA Extension ROM area stores ISA extension ROMs.  The BIOS will remap
 * PCI Option ROMs to this space to after PMM.  The first 64k of this space is
 * reserved for VGA adapters so normal devices start at 0xC8000.
 *
 * The BIOS Extension can map either additional ISA ROMs or portions of the BIOS
 * that didn't fit into the normal BIOS Area.
 *
 * This BIOS Area is the traditional location of the system BIOS.  The main
 * requirement is that the very last byte be valid as when the system starts
 * out, it jumps to this location to execute the BIOS.  Typically, you'll see
 * a trampoline loaded at the very end of the BIOS and the BIOS gets mapped with
 * it's end at 0xFFFFF.
 *
 * We setup the memory intercepts so that normal RAM accesses don't actually
 * trap to this function but we redirect anyway.  This is really to improve
 * code readability.
 */

void I440FX::mem_rw(uint64_t addr, unsigned len, uint64_t *data, bool read)
{
    if (addr_in_range(addr, len, 0x00000, 0xA0000)) {
        this->ram_rw(addr, len, data, read);
    } else if (addr_in_range(addr, len, 0xA0000, 0xC0000)) {
        this->rom_rw(addr, len, data, read);
    } else if (addr_in_range(addr, len, 0xC0000, 0xF0000)) {
        int segment = (addr - 0xC0000) / 0x4000 + 2;
        uint32_t enable_mask;

        if (read) {
            enable_mask = (segment & 0x01) ? PAM_RE_HIGH : PAM_RE_LOW;
        } else {
            enable_mask = (segment & 0x01) ? PAM_WE_HIGH : PAM_WE_LOW;
        }

        /* The PMC can be used to toggle whether reads and writes go to ROM or
         * main memory.  This allows for ROM shadowing.
         */
        if ((this->pam[segment / 2 + 1] & enable_mask)) {
            this->ram_rw(addr, len, data, read);
        } else {
            this->rom_rw(addr, len, data, read);
        }
    } else if (addr_in_range(addr, len, 0xF0000, 0xFFFFF)) {
        /* The addressing for the BIOS Area in the PAM registers is a little
         * different so we handle the case uniquely.
         */
        if ((this->pam[0] & (read ? PAM_RE_HIGH : PAM_WE_HIGH))) {
            this->ram_rw(addr, len, data, read);
        } else {
            this->rom_rw(addr, len, data, read);
        }
    } else if (this->addr_in_pci_range(addr, len)) {
        /* Forward accesses to PCI memory */
        this->pci_rw(addr, len, data, read);
    } else {
        this->ram_rw(addr, len, data, read);
    }
}

void I440FX::pio_rw(uint16_t addr, unsigned len, uint32_t *data, bool read)
{
    /* Only accesses as a dword are intercepted */
    if (addr == 0x0CF8 && len == 4) {
        if (read) {
            *data = this->confadd;
        } else {
            this->confadd = *data;
        }
        return;
    }

    if (addr == 0x0CFC && (this->confadd & CONFADD_CONE)) {
        this->confdata_rw(len, data, read);
        return;
    }

    /* The fall through here is important for correctness. */
    this->pci_pio_rw(addr, len, data, read);
}

void I440FX::reset(void)
{
    this->confadd = 0x00;

    this->command = 0x0006;
    this->status = 0x0280;
    this->mlt = 0x00;
    this->headt = 0x00;
    this->bist = 0x00;
    this->pmccfg = 0x00;
    this->deturbo = 0x00;
    this->dbc = 0x80;
    this->axc = 0x00;
    this->dramr = 0x0000;
    this->dramc = 0x01;
    this->dramt = 0x10;
    for (int i = 0; i < 7; i++) {
        this->pam[i] = 0x00;
    }
    /* Spec says default value is 0x01 but I see no advantage to this */
    for (int i = 0; i < 8; i++) {
        this->drb[i] = 0x00;
    }
    this->fdhc = 0x00;
    this->mtt = 0x00;
    this->clt = 0x10;
    this->smram = 0x20;
    this->errcmd = 0x00;
    this->errsts = 0x00;
    this->trc = 0x00;

    for (size_t i = 0; i < 31; i++) {
        if (this->slots[i]) {
            this->slots[i]->reset();
        }
    }
}

void I440FX::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "I440FX");

    marshal(m, "mem_intercepts", &this->mem_intercepts);

    marshal(m, "piix3", &this->piix3);
    marshal(m, "slots", &this->slots);
    marshal(m, "confadd", &this->confadd);

    marshal(m, "vendor_id", &this->vendor_id);
    marshal(m, "device_id", &this->device_id);
    marshal(m, "command", &this->command);
    marshal(m, "status", &this->status);
    marshal(m, "rid", &this->rid);
    marshal(m, "prog_if", &this->prog_if);
    marshal(m, "subclass_code", &this->subclass_code);
    marshal(m, "class_code", &this->class_code);
    marshal(m, "mlt", &this->mlt);
    marshal(m, "headt", &this->headt);
    marshal(m, "bist", &this->bist);
    marshal(m, "pmccfg", &this->pmccfg);
    marshal(m, "deturbo", &this->deturbo);
    marshal(m, "dbc", &this->dbc);
    marshal(m, "axc", &this->axc);
    marshal(m, "dramr", &this->dramr);
    marshal(m, "dramc", &this->dramc);
    marshal(m, "dramt", &this->dramt);
    marshal(m, "pam", &this->pam);
    marshal(m, "drb", &this->drb);
    marshal(m, "fdhc", &this->fdhc);
    marshal(m, "mtt", &this->mtt);
    marshal(m, "clt", &this->clt);
    marshal(m, "smram", &this->smram);
    marshal(m, "errcmd", &this->errcmd);
    marshal(m, "errsts", &this->errsts);
    marshal(m, "trc", &this->trc);

    m->end_struct();
}
