// PCI bus cannot be a PCI device.... or can it?

// Maybe we need PCIBase -> PCIBridgePCI
//               PCIBase -> PCIDevice

class PCIBus : public PCIDevice
{
public:
    PCIBus *find_bus(uint8_t bus_num);
    PCIDevice *find_dev(uint8_t bus_num, uint8_t devfn);
};

PCIBus *PCIBus::find_bus(uint8_t bus_num)
{
    /* It's me! */
    if (bus->secondary_bus == bus_num) {
        return bus;
    }

    /* It's not any of my children, get off my lawn! */
    if ((bus_num < dev->secondary_bus) || (bus_num > dev->subordinate_bus)) {
        return NULL;
    }

    for (int i = 0; i < 32; i++) {
        PCIDevice *dev;
        PCIBus *bus;

        /* Skip empty slots */
        if (!this->slots[i]) {
            continue;
        }

        /* Check the primary function if it's a bridge */
        if ((this->slots[i]->headt & PCI_HEADER_TYPE_PCI_BRIDGE)) {
            bus = dynamic_cast<PCIBus>(this->slots[i]);

            bus = bus->find_bus(bus_num);
            if (bus) {
                return bus;
            }
        }

        /* Not in the primary function, if it's a multifunction device,
         * search each function
         */
        if ((this->slots[i]->headt & PCI_HEADER_TYPE_MULTIFUNCTION)) {
            for (int j = 0; j < 7; j++) {
                if (!this->function[j]) {
                    continue;
                }

                if ((this->function[j]->headt & PCI_HEADER_TYPE_PCI_BRIDGE)) {
                    bus = dynamic_cast<PCIBus>(this->function[j]);

                    bus = bus->find_bus(bus_num);
                    if (bus) {
                        return bus;
                    }
                }
            }
        }
    }
    return NULL;
}

PCIDevice *PCIBus::find_dev(uint8_t bus_num, uint8_t devfn)
{
    uint8_t device = (devfn >> 3) & 0x1F;
    uint8_t fn = devfn & 0x07;
    PCIBus *bus;

    if (this->bus_num() != bus_num) {
        bus = this->find_bus(bus);
        if (bus == NULL) {
            return NULL;
        }

        return bus->find(dev);
    }

    if (!this->slots[device]) {
        return NULL;
    }

    dev = this->slots[device];
    if (fn == 0) {
        return dev;
    } else if (dev->functions[fn]) {
        return dev->functions[fn];
    }

    return NULL;
}
