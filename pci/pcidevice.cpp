class PCIDevice : public Device
{
public:
    void rw_config(uint8_t addr, unsigned len, uint32_t *data, bool read);

protected:
    uint16_t vendor_id;
    uint16_t device_id;
    uint16_t command;
    uint16_t status;
    uint8_t rid;
    uint8_t prog_if;
    uint8_t subclass_code;
    uint8_t class_code;
    uint8_t cls;
    uint8_t mlt;
    uint8_t headt;
    uint8_t bist;
    Array<uint32_t, 6> bar;
    uint32_t ccp;
    uint16_t subvendor_id;
    uint16_t subdevice_id;
    uint32_t rom_bar;
    uint8_t cap;
    uint8_t int_line;
    uint8_t int_pin;
    uint8_t min_gnt;
    uint8_t max_lat;
};

void PCIDevice::config_rw(uint8_t addr, unsigned len, uint32_t *data, bool read)
{
    RegisterIO reg(addr, len, data, read);

    /* These are required to be implemented */
    reg.io(&this->vendor_id, 0x00, READONLY);
    reg.io(&this->device_id, 0x02, READONLY);
    reg.io(&this->command, 0x04);
    reg.io(&this->status, 0x06);
    reg.io(&this->rid, 0x08, READONLY);
    reg.io(&this->prog_if, 0x09, READONLY);
    reg.io(&this->subclass_code, 0x0A, READONLY);
    reg.io(&this->class_code, 0x0B, READONLY);
    reg.io(&this->headt, 0x0E);
}


struct MemoryRegion
{
    uint64_t start;
    uint64_t size;
};

