class PCIDevice : public Device
{
public:
    void rw_config(uint8_t addr, unsigned len, uint32_t *data, bool read);

protected:
    uint16_t vendor_id;
    uint16_t device_id;
    uint16_t command;
    uint16_t status;
    uint8_t rid;
    uint8_t prog_if;
    uint8_t subclass_code;
    uint8_t class_code;
    uint8_t cls;
    uint8_t mlt;
    uint8_t headt;
    uint8_t bist;
    Array<uint32_t, 6> bar;
    uint32_t ccp;
    uint16_t subvendor_id;
    uint16_t subdevice_id;
    uint32_t rom_bar;
    uint8_t cap;
    uint8_t int_line;
    uint8_t int_pin;
    uint8_t min_gnt;
    uint8_t max_lat;
};

void PCIDevice::config_rw(uint8_t addr, unsigned len, uint32_t *data, bool read)
{
    RegisterIO reg(addr, len, data, read);

    /* These are required to be implemented */
    reg.io(&this->vendor_id, 0x00, READONLY);
    reg.io(&this->device_id, 0x02, READONLY);
    reg.io(&this->command, 0x04);
    reg.io(&this->status, 0x06);
    reg.io(&this->rid, 0x08, READONLY);
    reg.io(&this->prog_if, 0x09, READONLY);
    reg.io(&this->subclass_code, 0x0A, READONLY);
    reg.io(&this->class_code, 0x0B, READONLY);
    reg.io(&this->headt, 0x0E);
}


struct MemoryRegion
{
    uint64_t start;
    uint64_t size;
};

/**
 * The i440fx is the motherboard chipset that was popular in the Pentium Pro
 * era.  At this point in time, the chipset included both the Northbridge
 * (Memory Controller) and Southbridge (IO devices) functionality.
 *
 * The i440fx has a PCI 2.1 compatible bus and a PCI-to-ISA bridge.  All port
 * I/O and main memory access goes through the i440fx with the exception of
 * access to the local APIC which is part of the processor.
 *
 * The main interface to the i440fx is through the PCI interface.  The memory
 * controller is accessable as PCI device 0:0.0 even though it's technically
 * not a PCI device as the transactions for the device don't go over the PCI
 * bus.
 */
class I440FX : public Device
{
public:
    /**
     * Port I/O entry points.  It's expected that all PIO is sent through this
     * interface.
     */
    void pio_write(uint16_t addr, uint32_t data, unsigned len);
    uint32_t pio_read(uint16_t addr, unsigned len);

    /**
     * Memory entry points.  See mem_intercepts for usage.
     */
    void mem_write(uint64_t addr, uint64_t data, unsigned len);
    uint64_t mem_read(uint64_t addr, unsigned len);

    /**
     * Memory intercept API.  For performance reasons, instead of requiring all
     * memory accesses go through mem_write/mem_read, we provide a list of
     * regions that we care to go through our interface.
     */
    const std::vector<MemoryRegion> &get_mem_intercepts(void) const;

    void set_mem_change_notifier(std::function<void (void)> fn);

private:
    void confdata_write(uint32_t data, unsigned len);
    uint32_t confdata_read(unsigned len);

    void pmc_write(uint8_t regnum, uint32_t data, unsigned len);
    uint32_t pmc_read(uint8_t regnum, unsigned len);

    std::vector<MemoryRegion> mem_intercepts;
    std::function<void (void)> fn;

    uint32_t confadd;

    uint16_t vendor_id;
    uint16_t device_id;
    uint16_t command;
    uint16_t status;
    uint8_t rid;
    uint8_t prog_if;
    uint8_t subclass_code;
    uint8_t class_code;
    uint8_t mlt;
    uint8_t headt;
    uint8_t bist;
    uint16_t pmccfg;
    uint8_t deturbo;
    uint8_t dbc;
    uint8_t axc;
    uint16_t dramr;
    uint8_t dramc;
    uint8_t dramt;
    Array<uint8_t, 7> pam;
    Array<uint8_t, 8> drb;
    uint8_t fdhc;
    uint8_t mtt;
    uint8_t clt;
    uint8_t smram;
    uint8_t errcmd;
    uint8_t errsts;
    uint8_t trc;
};

void I440FX::reset(void)
{
    /* FIXME */
    this->confadd = 0x00;
    this->clt = 0x10;
    this->sram = 0x02;
}

void I440FX::update_ram_shadowing(void)
{
}

void I440FX::pmc_rw(uint8_t regnum, unsigned len, uint32_t *data, bool read)
{
    RegisterIO reg(regnum, len, data, read);

    reg.io(&this->vendor_id, 0x00, READONLY);
    reg.io(&this->device_id, 0x02, READONLY);
    reg.io(&this->command, 0x04, 0, 0x029F);
    reg.io(&this->status,  0x06, WRITECLEAR, 0x380);
    reg.io(&this->rid, 0x08, READ_ONLY);
    reg.io(&this->prog_if, 0x09, READONLY);
    reg.io(&this->subclass_code, 0x0A, READONLY);
    reg.io(&this->class_code, 0x0B, READONLY);
    reg.io(&this->mlt, 0x0D);
    reg.io(&this->headt, 0x0E);
    reg.io(&this->bist, 0x0F);

    reg.io(&this->pmccfg, 0x50, READWRITE, 0x4304);
    reg.io(&this->deturbo, 0x52);
    reg.io(&this->dbc, 0x53);
    reg.io(&this->axc, 0x54);
    reg.io(&this->dramr, 0x55);
    reg.io(&this->dramc, 0x57);
    reg.io(&this->dramt, 0x58);
    if (reg.io(&this->pam, 0x59)) {
        this->update_ram_shadowing();
    }
    reg.io(&this->drb, 0x60);
    reg.io(&this->fdhc, 0x68);
    reg.io(&this->mtt, 0x70);
    reg.io(&this->clt, 0x71);
    if (reg.io(&this->smram, 0x72)) {
        this->update_ram_shadowing();
    }
    reg.io(&this->errcmd, 0x90);
    reg.io(&this->errsts, 0x91, WRITECLEAR);
    reg.io(&this->trc, 0x93, WRITECLEAR);
}

void I440FX::confdata_write(uint32_t data, unsigned len)
{
    uint8_t bus = (data >> 16) & 0xFF;
    uint8_t devfn = (data >> 8) & 0xFF;
    uint8_t regnum = data & 0xFE;

    if (bus == 0 && devfn == 0) {
        this->pmc_rw(regnum, len, data, false);
        return;
    } else {
        PCIDevice *dev = this->find_dev(bus, devfn);
        if (dev) {
            dev->config_write(regnum, data, len);
        }
    }
}

uint32_t I440FX::confdata_read(unsigned len)
{
    if (bus == 0 && devfn == 0) {
        return this->pmc_rw(regnum, len, data, true);
    } else {
        PCIDevice *dev = this->find_dev(bus, devfn);
        if (dev) {
            return dev->config_read(regnum, data, len);
        }
    }

    return 0x00;
}

void I440FX::pio_write(uint16_t addr, uint32_t data, unsigned len)
{
    /* Only accesses as a dword are intercepted */
    if (addr == 0x0CF8 && len == 4) {
        this->confadd = data;
        return;
    }

    if (addr == 0x0CFC && (this->confadd & CONFADD_CONE)) {
        this->confdata_write(data, len);
        return;
    }

    /* FIXME search for any PCI device that claims this PIO port */
}

uint32_t I440FX::pio_read(uint16_t addr, unsigned len)
{
    /* Only accesses as a dword are intercepted */
    if (addr == 0x0CF8 && len == 4) {
        return this->condadd;
    }

    if (addr == 0x0CFC && (this->confadd & CONFADD_CONE)) {
        return this->confdata_read(len);
    }

    /* FIXME search for a PCI device that claims this port */

    return 0xFFFFFFFF;
}
