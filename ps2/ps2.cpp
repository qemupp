#include "ps2.hpp"

PS2Device::~PS2Device(void)
{
}

uint32_t PS2Device::read(void)
{
    int val, index;

    if (this->count == 0) {
        /* NOTE: if no data left, we return the last keyboard one
           (needed for EMM386) */
        /* XXX: need a timer to do things correctly */
        index = this->rptr - 1;
        if (index < 0) {
            index = PS2_QUEUE_SIZE - 1;
        }
        val = this->data[index];
    } else {
        val = this->data[this->rptr];
        if (++this->rptr == PS2_QUEUE_SIZE) {
            this->rptr = 0;
        }
        this->count--;
	if (this->count != 0) {
            this->bus->data_ready(this);
        }
    }
    return val;
}

bool PS2Device::queue_has_space(int count)
{
    if (this->count < (PS2_QUEUE_SIZE - count)) {
        return true;
    }
    return false;
}

bool PS2Device::can_read(void)
{
    return (this->count > 0);
}

void PS2Device::queue(int b)
{
    if (this->count >= PS2_QUEUE_SIZE) {
        return;
    }
    this->data[this->wptr] = b;
    if (++this->wptr == PS2_QUEUE_SIZE) {
        this->wptr = 0;
    }
    this->count++;
    this->bus->data_ready(this);
}

void PS2Device::reset(void)
{
    this->write_cmd = -1;
    this->rptr = 0;
    this->wptr = 0;
    this->count = 0;
}

void PS2Device::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "PS2Device");
    marshal(m, "write_cmd", &this->write_cmd);
    marshal(m, "data", &this->data);
    marshal(m, "rptr", &this->rptr);
    marshal(m, "wptr", &this->wptr);
    marshal(m, "count", &this->count);
    m->end_struct();
}
