#include "ps2_keyboard.hpp"

/* Keyboard Commands */
#define KBD_CMD_SET_LEDS	0xED	/* Set keyboard leds */
#define KBD_CMD_ECHO     	0xEE
#define KBD_CMD_SCANCODE	0xF0	/* Get/set scancode set */
#define KBD_CMD_GET_ID 	        0xF2	/* get keyboard ID */
#define KBD_CMD_SET_RATE	0xF3	/* Set typematic rate */
#define KBD_CMD_ENABLE		0xF4	/* Enable scanning */
#define KBD_CMD_RESET_DISABLE	0xF5	/* reset and disable scanning */
#define KBD_CMD_RESET_ENABLE   	0xF6    /* reset and enable scanning */
#define KBD_CMD_RESET		0xFF	/* Reset */

/* Keyboard Replies */
#define KBD_REPLY_POR		0xAA	/* Power on reset */
#define KBD_REPLY_ID		0xAB	/* Keyboard ID */
#define KBD_REPLY_ACK		0xFA	/* Command ACK */
#define KBD_REPLY_RESEND	0xFE	/* Command NACK, send the cmd again */

/* Table to convert from PC scancodes to raw scancodes.  */
static const unsigned char ps2_raw_keycode[128] = {
      0,118, 22, 30, 38, 37, 46, 54, 61, 62, 70, 69, 78, 85,102, 13,
     21, 29, 36, 45, 44, 53, 60, 67, 68, 77, 84, 91, 90, 20, 28, 27,
     35, 43, 52, 51, 59, 66, 75, 76, 82, 14, 18, 93, 26, 34, 33, 42,
     50, 49, 58, 65, 73, 74, 89,124, 17, 41, 88,  5,  6,  4, 12,  3,
     11,  2, 10,  1,  9,119,126,108,117,125,123,107,115,116,121,105,
    114,122,112,113,127, 96, 97,120,  7, 15, 23, 31, 39, 47, 55, 63,
     71, 79, 86, 94,  8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 87,111,
     19, 25, 57, 81, 83, 92, 95, 98, 99,100,101,103,104,106,109,110
};

PS2Keyboard::PS2Keyboard(void)
{
    this->reset();
}

void PS2Keyboard::write(int val)
{
    switch(this->write_cmd) {
    default:
    case -1:
        switch(val) {
        case 0x00:
            this->queue(KBD_REPLY_ACK);
            break;
        case 0x05:
            this->queue(KBD_REPLY_RESEND);
            break;
        case KBD_CMD_GET_ID:
            this->queue(KBD_REPLY_ACK);
            /* We emulate a MF2 AT keyboard here */
            this->queue(KBD_REPLY_ID);
            if (this->translate) {
                this->queue(0x41);
            } else {
                this->queue(0x83);
            }
            break;
        case KBD_CMD_ECHO:
            this->queue(KBD_CMD_ECHO);
            break;
        case KBD_CMD_ENABLE:
            this->scan_enabled = 1;
            this->queue(KBD_REPLY_ACK);
            break;
        case KBD_CMD_SCANCODE:
        case KBD_CMD_SET_LEDS:
        case KBD_CMD_SET_RATE:
            this->write_cmd = val;
            this->queue(KBD_REPLY_ACK);
            break;
        case KBD_CMD_RESET_DISABLE:
            this->reset_keyboard();
            this->scan_enabled = 0;
            this->queue(KBD_REPLY_ACK);
            break;
        case KBD_CMD_RESET_ENABLE:
            this->reset_keyboard();
            this->scan_enabled = 1;
            this->queue(KBD_REPLY_ACK);
            break;
        case KBD_CMD_RESET:
            this->reset_keyboard();
            this->queue(KBD_REPLY_ACK);
            this->queue(KBD_REPLY_POR);
            break;
        default:
            this->queue(KBD_REPLY_ACK);
            break;
        }
        break;
    case KBD_CMD_SCANCODE:
        if (val == 0) {
            if (this->scancode_set == 1) {
                this->put_keycode(0x43);
            } else if (this->scancode_set == 2) {
                this->put_keycode(0x41);
            } else if (this->scancode_set == 3) {
                this->put_keycode(0x3f);
            }
        } else {
            if (val >= 1 && val <= 3) {
                this->scancode_set = val;
            }
            this->queue(KBD_REPLY_ACK);
        }
        this->write_cmd = -1;
        break;
    case KBD_CMD_SET_LEDS:
        this->scroll_lock_led.set((val & 0x01));
        this->num_lock_led.set((val & 0x02));
        this->caps_lock_led.set((val & 0x04));
        this->queue(KBD_REPLY_ACK);
        this->write_cmd = -1;
        break;
    case KBD_CMD_SET_RATE:
        this->queue(KBD_REPLY_ACK);
        this->write_cmd = -1;
        break;
    }
}

void PS2Keyboard::put_keycode(int keycode)
{
    /* XXX: add support for scancode sets 1 and 3 */
    if (!this->translate && keycode < 0xe0 && this->scancode_set == 2) {
        if (keycode & 0x80) {
            this->queue(0xf0);
        }
        keycode = ps2_raw_keycode[keycode & 0x7f];
    }
    this->queue(keycode);
}

void PS2Keyboard::reset_keyboard(void)
{
    this->scan_enabled = 1;
    this->scancode_set = 2;
    this->scroll_lock_led.lower();
    this->num_lock_led.lower();
    this->caps_lock_led.lower();
}

void PS2Keyboard::set_translation(int mode)
{
    this->translate = mode;
}

void PS2Keyboard::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "PS2Keyboard");
    PS2Device::pickle(m, NULL);
    marshal(m, "scroll_lock_led", &this->scroll_lock_led);
    marshal(m, "num_lock_led", &this->num_lock_led);
    marshal(m, "caps_lock_led", &this->caps_lock_led);
    marshal(m, "scan_enabled", &this->scan_enabled);
    marshal(m, "translate", &this->translate);
    marshal(m, "scancode_set", &this->scancode_set);
    m->end_struct();
}

void PS2Keyboard::reset(void)
{
    PS2Device::reset();
    this->scroll_lock_led.lower();
    this->num_lock_led.lower();
    this->caps_lock_led.lower();
    this->scan_enabled = 0;
    this->translate = 0;
    this->scancode_set = 0;
}
