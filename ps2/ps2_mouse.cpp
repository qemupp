#include "ps2_mouse.hpp"

/* Mouse Commands */
#define AUX_SET_SCALE11		0xE6	/* Set 1:1 scaling */
#define AUX_SET_SCALE21		0xE7	/* Set 2:1 scaling */
#define AUX_SET_RES		0xE8	/* Set resolution */
#define AUX_GET_SCALE		0xE9	/* Get scaling factor */
#define AUX_SET_STREAM		0xEA	/* Set stream mode */
#define AUX_POLL		0xEB	/* Poll */
#define AUX_RESET_WRAP		0xEC	/* Reset wrap mode */
#define AUX_SET_WRAP		0xEE	/* Set wrap mode */
#define AUX_SET_REMOTE		0xF0	/* Set remote mode */
#define AUX_GET_TYPE		0xF2	/* Get type */
#define AUX_SET_SAMPLE		0xF3	/* Set sample rate */
#define AUX_ENABLE_DEV		0xF4	/* Enable aux device */
#define AUX_DISABLE_DEV		0xF5	/* Disable aux device */
#define AUX_SET_DEFAULT		0xF6
#define AUX_RESET		0xFF	/* Reset aux device */
#define AUX_ACK			0xFA	/* Command byte ACK. */

#define MOUSE_STATUS_REMOTE     0x40
#define MOUSE_STATUS_ENABLED    0x20
#define MOUSE_STATUS_SCALE21    0x10

PS2Mouse::PS2Mouse(void)
{
    this->reset();
}

void PS2Mouse::send_packet(void)
{
    unsigned int b;
    int dx1, dy1, dz1;

    dx1 = this->dx;
    dy1 = this->dy;
    dz1 = this->dz;
    /* XXX: increase range to 8 bits ? */
    if (dx1 > 127) {
        dx1 = 127;
    } else if (dx1 < -127) {
        dx1 = -127;
    }
    if (dy1 > 127) {
        dy1 = 127;
    } else if (dy1 < -127) {
        dy1 = -127;
    }
    b = 0x08 | ((dx1 < 0) << 4) | ((dy1 < 0) << 5) | (this->buttons & 0x07);
    this->queue(b);
    this->queue(dx1 & 0xff);
    this->queue(dy1 & 0xff);
    /* extra byte for IMPS/2 or IMEX */
    switch(this->type) {
    default:
        break;
    case 3:
        if (dz1 > 127) {
            dz1 = 127;
        } else if (dz1 < -127) {
                dz1 = -127;
        }
        this->queue(dz1 & 0xff);
        break;
    case 4:
        if (dz1 > 7) {
            dz1 = 7;
        } else if (dz1 < -7) {
            dz1 = -7;
        }
        b = (dz1 & 0x0f) | ((this->buttons & 0x18) << 1);
        this->queue(b);
        break;
    }

    /* update deltas */
    this->dx -= dx1;
    this->dy -= dy1;
    this->dz -= dz1;
}

void PS2Mouse::event(int dx, int dy, int dz, int buttons_state)
{
    /* check if deltas are recorded when disabled */
    if (!(this->status & MOUSE_STATUS_ENABLED)) {
        return;
    }

    this->dx += dx;
    this->dy -= dy;
    this->dz += dz;
    /* XXX: SDL sometimes generates nul events: we delete them */
    if (this->dx == 0 && this->dy == 0 && this->dz == 0 &&
        this->buttons == buttons_state) {
	return;
    }
    this->buttons = buttons_state;

    if (!(this->status & MOUSE_STATUS_REMOTE) && this->queue_has_space(16)) {
        for (;;) {
            /* if not remote, send event. Multiple events are sent if
               too big deltas */
            this->send_packet();
            if (this->dx == 0 && this->dy == 0 && this->dz == 0) {
                break;
            }
        }
    }
}

void PS2Mouse::write(int val)
{
    switch (this->write_cmd) {
    default:
    case -1:
        /* mouse command */
        if (this->wrap) {
            if (val == AUX_RESET_WRAP) {
                this->wrap = 0;
                this->queue(AUX_ACK);
                return;
            } else if (val != AUX_RESET) {
                this->queue(val);
                return;
            }
        }
        switch (val) {
        case AUX_SET_SCALE11:
            this->status &= ~MOUSE_STATUS_SCALE21;
            this->queue(AUX_ACK);
            break;
        case AUX_SET_SCALE21:
            this->status |= MOUSE_STATUS_SCALE21;
            this->queue(AUX_ACK);
            break;
        case AUX_SET_STREAM:
            this->status &= ~MOUSE_STATUS_REMOTE;
            this->queue(AUX_ACK);
            break;
        case AUX_SET_WRAP:
            this->wrap = 1;
            this->queue(AUX_ACK);
            break;
        case AUX_SET_REMOTE:
            this->status |= MOUSE_STATUS_REMOTE;
            this->queue(AUX_ACK);
            break;
        case AUX_GET_TYPE:
            this->queue(AUX_ACK);
            this->queue(this->type);
            break;
        case AUX_SET_RES:
        case AUX_SET_SAMPLE:
            this->write_cmd = val;
            this->queue(AUX_ACK);
            break;
        case AUX_GET_SCALE:
            this->queue(AUX_ACK);
            this->queue(this->status);
            this->queue(this->resolution);
            this->queue(this->sample_rate);
            break;
        case AUX_POLL:
            this->queue(AUX_ACK);
            this->send_packet();
            break;
        case AUX_ENABLE_DEV:
            this->status |= MOUSE_STATUS_ENABLED;
            this->queue(AUX_ACK);
            break;
        case AUX_DISABLE_DEV:
            this->status &= ~MOUSE_STATUS_ENABLED;
            this->queue(AUX_ACK);
            break;
        case AUX_SET_DEFAULT:
            this->sample_rate = 100;
            this->resolution = 2;
            this->status = 0;
            this->queue(AUX_ACK);
            break;
        case AUX_RESET:
            this->sample_rate = 100;
            this->resolution = 2;
            this->status = 0;
            this->type = 0;
            this->queue(AUX_ACK);
            this->queue(0xaa);
            this->queue(this->type);
            break;
        default:
            break;
        }
        break;
    case AUX_SET_SAMPLE:
        this->sample_rate = val;
        /* detect IMPS/2 or IMEX */
        switch (this->detect_state) {
        default:
        case 0:
            if (val == 200) {
                this->detect_state = 1;
            }
            break;
        case 1:
            if (val == 100) {
                this->detect_state = 2;
            } else if (val == 200) {
                this->detect_state = 3;
            } else {
                this->detect_state = 0;
            }
            break;
        case 2:
            if (val == 80) {
                this->type = 3; /* IMPS/2 */
            }
            this->detect_state = 0;
            break;
        case 3:
            if (val == 80) {
                this->type = 4; /* IMEX */
            }
            this->detect_state = 0;
            break;
        }
        this->queue(AUX_ACK);
        this->write_cmd = -1;
        break;
    case AUX_SET_RES:
        this->resolution = val;
        this->queue(AUX_ACK);
        this->write_cmd = -1;
        break;
    }
}

void PS2Mouse::reset(void)
{
    PS2Device::reset();
    this->status = 0;
    this->resolution = 0;
    this->sample_rate = 0;
    this->wrap = 0;
    this->type = 0;
    this->detect_state = 0;
    this->dx = 0;
    this->dy = 0;
    this->dz = 0;
    this->buttons = 0;
}

void PS2Mouse::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "PS2Mouse");
    PS2Device::pickle(m, NULL);
    marshal(m, "status", &this->status);
    marshal(m, "resolution", &this->resolution);
    marshal(m, "sample_rate", &this->sample_rate);
    marshal(m, "wrap", &this->wrap);
    marshal(m, "dx", &this->dx);
    marshal(m, "dy", &this->dy);
    marshal(m, "dz", &this->dz);
    marshal(m, "buttons", &this->buttons);
    m->end_struct();
}
