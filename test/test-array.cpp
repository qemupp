#include "util.hpp"

#include <stdio.h>

int main(int argc, char **argv)
{
    Array<int, 10> array;
    bool error;

    array[0] = 10;
    array[1] = 20;
    array[3] = 43;

    try {
        error = false;
        array[11] = 32;
        error = true;
    } catch (std::exception *e) {
        delete e;
    }

    if (error) {
        printf("FAIL: out of bounds write succeeded\n");
        return 1;
    }

    return 0;
}
