#include "ps2_keyboard.hpp"
#include "i8042.hpp"
#include "util.hpp"

#include <stdio.h>

/* Table to convert from PC scancodes to raw scancodes.  */
static const unsigned char ps2_raw_keycode[128] = {
      0,118, 22, 30, 38, 37, 46, 54, 61, 62, 70, 69, 78, 85,102, 13,
     21, 29, 36, 45, 44, 53, 60, 67, 68, 77, 84, 91, 90, 20, 28, 27,
     35, 43, 52, 51, 59, 66, 75, 76, 82, 14, 18, 93, 26, 34, 33, 42,
     50, 49, 58, 65, 73, 74, 89,124, 17, 41, 88,  5,  6,  4, 12,  3,
     11,  2, 10,  1,  9,119,126,108,117,125,123,107,115,116,121,105,
    114,122,112,113,127, 96, 97,120,  7, 15, 23, 31, 39, 47, 55, 63,
     71, 79, 86, 94,  8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 87,111,
     19, 25, 57, 81, 83, 92, 95, 98, 99,100,101,103,104,106,109,110
};

int main(int argc, char **argv)
{
    I8042 i8042;
    PS2Keyboard kbd;
    Pin vcc(1);

    i8042.insert_kbd(&kbd);
    i8042.vcc = &vcc;

    /* kbd echo */
    i8042.write_data(0xee);
    if (i8042.read_data() != 0xee) {
        printf("FAIL: keyboard echo failed\n");
        return 1;
    }

    /* kbd get id */
    i8042.write_data(0xf2);
    if (i8042.read_data() != 0xfa || i8042.read_data() != 0xab || i8042.read_data() != 0x83) {
        printf("FAIL: keyboard did not return MF2 AT keyboard ID\n");
        return 1;
    }

    /* reset */
    i8042.write_data(0xff);
    if (i8042.read_data() != 0xfa || i8042.read_data() != 0xaa) {
        printf("FAIL: failed to reset the keyboard\n");
        return 1;
    }

    for (unsigned int i = 0; i < 0x7F; i++) {
        unsigned int rsp;
        kbd.put_keycode(i);
        rsp = i8042.read_data();

        if (ps2_raw_keycode[i] != rsp) {
            printf("FAIL: got response %d from keyboard, expected %d\n",
                   rsp, ps2_raw_keycode[i]);
            return 1;
        }
    }

    return 0;
}
