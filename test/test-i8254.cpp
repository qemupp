#include <getopt.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include "i8254.hpp"
#include "test-time-source.hpp"
#include "util.hpp"
#include "timelib.hpp"

static void pit_write_periodic_counter(PIT *pit, int counter, uint16_t value)
{
    /* write to control register to enable counter in mode 2 using
     * lsb/msb rw mode */
    pit->write(0x03, (counter << 6) | 0x34);

    /* write LSB to counter */
    pit->write(counter, value & 0xFF);

    /* write MSB to counter */
    pit->write(counter, (value >> 8) & 0xFF);
}

static void pit_write_enable_oneshot(PIT *pit, int counter)
{
    /* write to control register to enable counter in mode 2 using
     * lsb/msb rw mode */
    pit->write(0x03, (counter << 6) | 0x38);
}

static void pit_write_next_oneshot(PIT *pit, int counter, uint16_t value)
{
    /* write LSB to counter */
    pit->write(counter, value & 0xFF);

    /* write MSB to counter */
    pit->write(counter, (value >> 8) & 0xFF);
}

#if 0
static uint16_t pit_read_counter(PIT *pit, int counter)
{
    uint16_t val;

    /* latch counter */
    pit->write(0x03, (counter << 6) | 0x04);

    /* read LSB */
    val = pit->read(0x00);

    /* read MSB */
    val |= (pit->read(0x00) << 8);

    return val;
}
#endif

static uint16_t usec_to_pit_ticks(uint64_t val)
{
    return muldiv64(val, PIT_FREQ, USEC_PER_SEC);
}

static int interrupt_count;

static void irq0(void)
{
    interrupt_count++;
}

static bool test_mode2(PIT *pit, TestTimeSource *ts)
{
    uint64_t frequency = randint(10, 1024);
    int expected_ints = randint(100, 1000);
    uint64_t pit_freq = usec_to_pit_ticks(USEC_PER_SEC / frequency);
    uint64_t step_rate = randint(pit_freq / 10, pit_freq);
    uint64_t base;

    printf("Testing mode 2 periodic timer...\n");

    pit_write_periodic_counter(pit, 0, pit_freq);

    base = now(PC_FREQ0);

    /* loop for the given period of time */
    interrupt_count = 0;
    /* we're having a problem when <= is only <.  It's non immediately obvious
     * why though.  See FAILS.
     * FIXME */
    while (now(PC_FREQ0) <= base + (expected_ints * pit_freq)) {
        ts->advance(step_rate, PC_FREQ0);
    }

    if (interrupt_count != expected_ints) {
        printf("FAIL: expected %d interrupts, got %d\n",
               expected_ints, interrupt_count);
        printf("frequency: %ld\n"
               "pit_freq: %ld\n"
               "step_rate: %ld\n",
               frequency, pit_freq, step_rate);
        return false;
    }

    return true;
}

static bool test_mode4(PIT *pit, TestTimeSource *ts, uint64_t timeout)
{
    uint64_t now_ts;

    interrupt_count = 0;

    now_ts = now(PC_FREQ0);
    pit_write_next_oneshot(pit, 0, timeout);

    while (now(PC_FREQ0) < (now_ts + timeout)) {
        if (interrupt_count) {
            printf("FAIL: unexpected oneshot interrupt\n");
            return 1;
        }
        ts->advance(randint(timeout / 10, timeout / 2), PC_FREQ0);
    }

    if (!interrupt_count) {
        printf("FAIL: missing oneshot interrupt\n");
        return false;
    }

    return true;
}

int main(int argc, char **argv)
{
    const char *sopts = "ht:pgdf";
    struct option lopts[] = {
        { "help", 0, 0, 'h' },
        { "timestamp", 1, 0, 't' },
        { "precise-ts", 0, 0, 'p' },
	{ "gradual", 0, 0, 'g' },
	{ "drop", 0, 0, 'd' },
	{ "fast", 0, 0, 'f' },
        {},
    };
    int ch;
    int opt_ind = 0;
    time_t timestamp = gettimestamp();
    bool precise_ts = false;
    DriftMode mode = DRIFT_REINJECT_FAST;

    while ((ch = getopt_long(argc, argv, sopts, lopts, &opt_ind)) != -1) {
        switch (ch) {
        case 't':
            timestamp = atoi(optarg);
            break;
        case 'p':
            precise_ts = true;
            break;
	case 'g':
            mode = DRIFT_REINJECT_GRADUAL;
            break;
        case 'd':
            mode = DRIFT_DROP;
            break;
        case 'f':
            mode = DRIFT_REINJECT_FAST;
            break;
        case 'h':
            exit(0);
        }
    }

    TestTimeSource ts(timestamp, SEC, precise_ts);
    Pin vcc(true);

    register_time_source(&ts);

    srandom(timestamp);

    printf("%s --timestamp=%ld%s%s%s\n", argv[0], timestamp,
           precise_ts ? " --precise-ts" : "",
	   (mode == DRIFT_REINJECT_GRADUAL) ? " --gradual" : "",
           (mode == DRIFT_DROP) ? " --drop" : "");

    PIT pit(mode);
    pit.vcc = &vcc;

    pit.get_irq().connect(Pin::RISING_EDGE, irq0);

    if (!test_mode2(&pit, &ts)) {
        return 1;
    }

    /* FIXME test oneshot timer */
    pit_write_enable_oneshot(&pit, 0);

    printf("Testing oneshot timer...\n");

    int count = randint(3, 50);

    for (int i = 0; i < count; i++) {
        uint64_t pit_ticks = usec_to_pit_ticks(1000);
        if (!test_mode4(&pit, &ts, randint(pit_ticks / 10, pit_ticks))) {
            return 1;
        }
    }

    printf("Testing drift catchup...\n");

    uint64_t pit_freq = usec_to_pit_ticks(USEC_PER_SEC / 1024);
    pit_write_periodic_counter(&pit, 0, pit_freq);

    if (!check_drift(&ts, pit_freq, PC_FREQ0, mode, &interrupt_count)) {
        return 1;
    }

    return 0;
}
