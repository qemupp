#include "ps2_keyboard.hpp"
#include "util.hpp"

#include <stdio.h>

/* Table to convert from PC scancodes to raw scancodes.  */
static const unsigned char ps2_raw_keycode[128] = {
      0,118, 22, 30, 38, 37, 46, 54, 61, 62, 70, 69, 78, 85,102, 13,
     21, 29, 36, 45, 44, 53, 60, 67, 68, 77, 84, 91, 90, 20, 28, 27,
     35, 43, 52, 51, 59, 66, 75, 76, 82, 14, 18, 93, 26, 34, 33, 42,
     50, 49, 58, 65, 73, 74, 89,124, 17, 41, 88,  5,  6,  4, 12,  3,
     11,  2, 10,  1,  9,119,126,108,117,125,123,107,115,116,121,105,
    114,122,112,113,127, 96, 97,120,  7, 15, 23, 31, 39, 47, 55, 63,
     71, 79, 86, 94,  8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 87,111,
     19, 25, 57, 81, 83, 92, 95, 98, 99,100,101,103,104,106,109,110
};

class TestPS2Controller : public PS2Controller
{
public:
    void data_ready(PS2Device *device) {
    }
};

int main(int argc, char **argv)
{
    PS2Keyboard kbd;
    TestPS2Controller test_controller;
    Pin vcc(1);

    kbd.vcc = &vcc;
    kbd.bus = &test_controller;

    /* kbd echo */
    kbd.write(0xee);
    if (kbd.read() != 0xee) {
        printf("FAIL: keyboard echo failed\n");
        return 1;
    }

    /* kbd get id */
    kbd.write(0xf2);
    if (kbd.read() != 0xfa || kbd.read() != 0xab || kbd.read() != 0x83) {
        printf("FAIL: keyboard did not return MF2 AT keyboard ID\n");
        return 1;
    }

    /* reset */
    kbd.write(0xff);
    if (kbd.read() != 0xfa || kbd.read() != 0xaa) {
        printf("FAIL: failed to reset the keyboard\n");
        return 1;
    }

    for (unsigned int i = 0; i < 0x7F; i++) {
        unsigned int rsp;
        kbd.put_keycode(i);
        rsp = kbd.read();

        if (ps2_raw_keycode[i] != rsp) {
            printf("FAIL: got response %d from keyboard, expected %d\n",
                   rsp, ps2_raw_keycode[i]);
            return 1;
        }
    }
    

    return 0;
}
