#include "xmlwriter.hpp"
#include "mc146818a.hpp"
#include "test-time-source.hpp"

#include <time.h>
#include <stdio.h>

class Foo
{
public:
    Foo(int x, int y, int z) : x(x), y(y), z(z) {
        time_t ts = time(NULL);
        localtime_r(&ts, &this->tm);
    }

    void pickle(Marshaller *m, const char *name) {
        m->start_struct(name, "Foo");
        marshal(m, "tm", &tm);
        marshal(m, "x", &x);
        marshal(m, "y", &y);
        marshal(m, "z", &z);
        m->end_struct();
    }

private:
    struct tm tm;
    int x;
    int y;
    int z;
};

int main(int argc, char **argv)
{
    XMLWriter m;
    struct tm tm;
    time_t ts;
    Foo foo(2, 3, 4);
    TestTimeSource tmsrc(1, SEC);

    register_time_source(&tmsrc);

    ts = time(NULL);
    localtime_r(&ts, &tm);

    RTC rtc(1980, &tm);

    marshal(&m, "tm", &tm);
    marshal(&m, "foo", &foo);
    marshal(&m, "rtc", &rtc);

    printf("%s\n", m.as_string().c_str());

    return 0;
}
