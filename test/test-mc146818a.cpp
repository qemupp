#include <getopt.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include <functional>

#include "mc146818a.hpp"
#include "mc146818a_def.hpp"
#include "test-time-source.hpp"
#include "util.hpp"
#include "timelib.hpp"

static void tm_advance(struct tm *tm, time_t *timestamp, int seconds)
{
    *timestamp += seconds;
    gmtime_r(timestamp, tm);
}

static uint8_t bcd2dec(uint8_t value)
{
    return (((value >> 4) & 0x0F) * 10) + (value & 0x0F);
}

static uint8_t dec2bcd(uint8_t value)
{
    return ((value / 10) << 4) | (value % 10);
}

static bool check_time(RTC *rtc, int32_t base_year, struct tm *tm)
{
    int sec, min, hour, hour_offset;
    int wday, mday, mon, year;
    static const char *dow[] = {
        "Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"
    };

    sec = rtc->cmos_read(0x00);
    min = rtc->cmos_read(0x02);
    hour = rtc->cmos_read(0x04);
    wday = rtc->cmos_read(0x06);
    mday = rtc->cmos_read(0x07);
    mon = rtc->cmos_read(0x08);
    year = rtc->cmos_read(0x09);

    if ((rtc->cmos_read(0x0B) & 4) == 0) {
        sec = bcd2dec(sec);
        min = bcd2dec(min);
        hour = bcd2dec(hour);
        wday = bcd2dec(wday);
        mday = bcd2dec(mday);
        mon = bcd2dec(mon);
        year = bcd2dec(year);
        hour_offset = 80;
    } else {
        hour_offset = 0x80;
    }


    if ((rtc->cmos_read(0x0B) & 2) == 0) {
        if (hour >= hour_offset) {
            hour -= hour_offset;
            hour += 12;
        }
    }

    if ((sec == tm->tm_sec) &&
        (min == tm->tm_min) &&
        (hour == tm->tm_hour) &&
        (wday == tm->tm_wday + 1) &&
        (mday == tm->tm_mday) &&
        (mon == tm->tm_mon + 1) &&
        (year == (tm->tm_year + 1900 - base_year))) {
        return true;
    }

    printf("%s %02d/%02d/%04d %02d:%02d:%02d == ",
           dow[wday - 1], mon, mday, base_year + year, hour, min, sec);

    printf("%s %02d/%02d/%04d %02d:%02d:%02d\n",
           dow[tm->tm_wday], tm->tm_mon + 1, tm->tm_mday,
           1900 + tm->tm_year, tm->tm_hour, tm->tm_min, tm->tm_sec);

    return false;
}

static bool check_irq(RTC *rtc)
{
    if (rtc->irq.get()) {
        return true;
    }
    return false;
}

static void set_alarm_time(RTC *rtc, struct tm *tm)
{
    int sec, min, hour;
    int hour_offset;

    sec = tm->tm_sec;
    min = tm->tm_min;
    hour = tm->tm_hour;

    if ((rtc->cmos_read(0x0B) & 4) == 0) {
        sec = dec2bcd(sec);
        min = dec2bcd(min);
        hour = dec2bcd(hour);
        hour_offset = 80;
    } else {
        hour_offset = 0x80;
    }

    if ((rtc->cmos_read(0x0B) & 2) == 0) {
        if (hour >= 12) {
            hour -= 12;
            hour += hour_offset;
        }
    }

    rtc->cmos_write(0x01, sec);
    rtc->cmos_write(0x03, min);
    rtc->cmos_write(0x05, hour);
}

static bool test_periodic_timer(TestTimeSource &ts, RTC &rtc, int period)
{
    int tick_count = 0;

    for (int i = 0; i < 40000; i++) {
        ts.advance(50, USEC);
        if (rtc.irq.get()) {
            tick_count++;
            rtc.cmos_read(0x0C); /* EOI */
        }
    }

    if (tick_count != 2 * period) {
        printf("FAIL: expected %d ticks from periodic timer, got %d\n",
               2 * period, tick_count);
        return false;
    }

    return 1;
}

static int interrupt_count;

static void irq_fire(RTC *rtc)
{
    interrupt_count++;
    rtc->cmos_read(0x0C); /* EOI */
}

int main(int argc, char **argv)
{
    const char *sopts = "ht:b:gdf";
    struct option lopts[] = {
        { "help", 0, 0, 'h' },
        { "timestamp", 1, 0, 't' },
        { "base-year", 1, 0, 'b' },
	{ "gradual", 0, 0, 'g' },
	{ "drop", 0, 0, 'd' },
	{ "fast", 0, 0, 'f' },
        {},
    };
    struct tm tm;
    int ch;
    int opt_ind = 0;
    time_t timestamp = gettimestamp();
    int32_t base_year = 1980;
    DriftMode drift_mode = DRIFT_REINJECT_FAST;

    while ((ch = getopt_long(argc, argv, sopts, lopts, &opt_ind)) != -1) {
        switch (ch) {
        case 't':
            timestamp = atoi(optarg);
            break;
        case 'b':
            base_year = atoi(optarg);
            break;
	case 'g':
            drift_mode = DRIFT_REINJECT_GRADUAL;
            break;
        case 'd':
            drift_mode = DRIFT_DROP;
            break;
        case 'f':
            drift_mode = DRIFT_REINJECT_FAST;
            break;
        case 'h':
            exit(0);
        }
    }

    TestTimeSource ts(timestamp, SEC);

    register_time_source(&ts);

    srandom(timestamp);

    printf("%s --timestamp=%ld --base-year=%d%s%s\n",
           argv[0], timestamp, base_year,
           (drift_mode == DRIFT_REINJECT_GRADUAL) ? " --gradual" : "",
           (drift_mode == DRIFT_DROP) ? " --drop" : "");

    gmtime_r(&timestamp, &tm);

    RTC rtc(base_year, &tm, drift_mode);
    Pin vcc(1);

    rtc.vcc = &vcc;

    printf("CMOS:\n");
    printf(" ");
    for (int i = 0; i < 128; i++) {
        if (i && !(i % 24)) {
            printf("\n ");
        }
        printf("%02X ", rtc.cmos_read(i));
    }
    printf("\n");

    printf("Checking time handling...\n");

    if (!check_time(&rtc, base_year, &tm)) {
        printf("FAIL: checking initial time\n");
        return 1;
    }

    if (check_irq(&rtc)) {
        printf("FAIL: unexpected interrupt\n");
        return 1;
    }

    ts.advance(1, SEC);
    tm_advance(&tm, &timestamp, 1);

    if (!check_time(&rtc, base_year, &tm)) {
        printf("FAIL: time not match after 1 second\n");
        return 1;
    }

    if (check_irq(&rtc)) {
        printf("FAIL: unexpected interrupt\n");
        return 1;
    }

    /* this is a weird behavior and i'm not sure if this is right or we're just
       lucky that we get away with it */
    rtc.cmos_write(0x0B, 0x00); /* set to binary mode */

    if (!check_time(&rtc, base_year, &tm)) {
        printf("FAIL: time not match after 0 second in binary mode\n");
        return 1;
    }

    ts.advance(1, SEC);
    tm_advance(&tm, &timestamp, 1);
    
    if (!check_time(&rtc, base_year, &tm)) {
        printf("FAIL: time not match after 1 second in binary mode\n");
        return 1;
    }

    if (check_irq(&rtc)) {
        printf("FAIL: unexpected interrupt\n");
        return 1;
    }

    rtc.cmos_write(0x0B, 0x02); /* set back to BCD mode */
    ts.advance(1, SEC);
    tm_advance(&tm, &timestamp, 1);
        
    if (!check_time(&rtc, base_year, &tm)) {
        printf("FAIL: time not match after 1 second in BCD mode\n");
        return 1;
    }

    if (check_irq(&rtc)) {
        printf("FAIL: unexpected interrupt\n");
        return 1;
    }

    printf("Checking date interrupt...\n");

    for (int i = 0; i < 10; i++) {
        int v = randint(1, 100000);

        ts.advance(v, SEC);
        tm_advance(&tm, &timestamp, v);

        if (!check_time(&rtc, base_year, &tm)) {
            printf("FAIL: time not match after advancement\n");
            return 1;
        }

        if (check_irq(&rtc)) {
            printf("FAIL: unexpected interrupt\n");
            return 1;
        }
    }

    tm_advance(&tm, &timestamp, 10);
    set_alarm_time(&rtc, &tm);
    rtc.cmos_write(0x0B, rtc.cmos_read(0x0B) | (1 << 5));

    for (int i = 0; i < 9; i++) {
        ts.advance(1, SEC);
        if (check_irq(&rtc)) {
            printf("FAIL: received interrupt earlier than expected\n");
            return 1;
        }
    }

    ts.advance(1, SEC);

    if (!check_irq(&rtc)) {
        printf("FAIL: failed to deliver interrupt on date match\n");
        return 1;
    }

    rtc.cmos_read(0x0C); /* EOI for interrupt */

    if (check_irq(&rtc)) {
        printf("FAIL: rtc didn't clear interrupt when expected\n");
        return 1;
    }

    rtc.cmos_write(0x0B, rtc.cmos_read(0x0B) & ~(1 << 5));

    printf("checking periodic timer...\n");
    rtc.cmos_write(0x0B, rtc.cmos_read(0x0B) | (1 << 6));
    rtc.cmos_write(0x0A, 0x26); /* select 1 Khz timer */

    tm_advance(&tm, &timestamp, 2);
    if (!test_periodic_timer(ts, rtc, 1024)) {
        return 1;
    }

    tm_advance(&tm, &timestamp, 2);
    if (!test_periodic_timer(ts, rtc, 1024)) {
        return 1;
    }

    rtc.cmos_write(0x0A, 0x2F);
    tm_advance(&tm, &timestamp, 2);
    if (!test_periodic_timer(ts, rtc, 2)) {
        return 1;
    }

    rtc.cmos_write(0x0B, rtc.cmos_read(0x0B) & ~(1 << 6));
    /* writes to A register always ors results?  that
     * seems wrong... */

    ts.advance(999999, USEC);
    tm_advance(&tm, &timestamp, 1);

    if (!check_time(&rtc, base_year, &tm)) {
        printf("FAIL: time not match after advancement\n");
        return 1;
    }

    rtc.reset();

    rtc.cmos_write(0x0B, rtc.cmos_read(0x0B) | (1 << 6));
    rtc.cmos_write(0x0A, 0x26); /* select 1 Khz timer */

    printf("Checking time drift...\n");
    rtc.irq.connect(Pin::RISING_EDGE, std::bind(irq_fire, &rtc));

    if (!check_drift(&ts, 32, PC_FREQ1, drift_mode, &interrupt_count)) {
        return 1;
    }

    return 0;
}
