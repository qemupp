#include <stdio.h>

#include "regio.hpp"

static bool check_io(uint32_t *foo, unsigned size)
{
    uint32_t val, expected;
    RegisterIO reg(0x40, size, &val, true);

    expected = (*foo & ((1ULL << (size * 8)) - 1));

    reg.io(foo, 0x40);
    if (val != expected) {
        printf("FAIL: expected %x, got %x\n", expected, val);
        return false;
    }

    return true;
}

int main(int argc, char **argv)
{
    uint32_t foo = 0x12345678;
    int i;

    for (i = 0; i < 3; i++) {
        if (!check_io(&foo, (1 << i))) {
            return 1;
        }
    }

    return 0;
}
