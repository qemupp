#include "southbridge.hpp"
#include "test-time-source.hpp"
#include "xmlwriter.hpp"
#include "timelib.hpp"

#include <getopt.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    const char *sopts = "hst:";
    struct option lopts[] = {
        { "help", 0, 0, 'h' },
        { "timestamp", 1, 0, 't' },
        { "schema-only", 0, 0, 's' },
        {},
    };
    struct tm tm;
    time_t ts = gettimestamp();
    int ch;
    int opt_ind = 0;
    bool schema = false;

    while ((ch = getopt_long(argc, argv, sopts, lopts, &opt_ind)) != -1) {
        switch (ch) {
        case 't':
            ts = atoi(optarg);
            break;
	case 's':
            schema = true;
            break;
        case 'h':
            exit(0);
        }
    }

    TestTimeSource src(ts, SEC);

    register_time_source(&src);

    gmtime_r(&ts, &tm);

    SouthBridge sb(1980, &tm, DRIFT_REINJECT_GRADUAL);
    XMLWriter xw(schema);

    sb.realize();

    marshal(&xw, "sb", &sb);

    printf("%s", xw.as_string().c_str());

    return 0;
}
