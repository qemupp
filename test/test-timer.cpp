#include <functional>
#include <stdio.h>
#include "timer.hpp"
#include "test-time-source.hpp"

static bool timer_fired = false;

static void fired(void)
{
    timer_fired = true;
}

int main(int argc, char **argv)
{
    uint64_t t = 10000;
    TestTimeSource ts(t, SEC);
    Timer timer;

    register_time_source(&ts);

    timer.set(fired);
    timer.update(t + 10, SEC);

    for (int i = 0; i < 10; i++) {
        if (timer_fired) {
            printf("FAIL: timer misfire after %d secs\n", i);
            return 1;
        }
        ts.advance(1, SEC);
    }

    if (!timer_fired) {
        printf("FAIL: timer did not fire when expected\n");
        return 1;
    }

    timer_fired = 0;
    timer.update(ts.now(SEC) + 5, SEC);

    for (int i = 0; i < 10; i++) {
        if (timer_fired) {
            printf("FAIL: timer misfire after %d half-secs\n", 20 + i);
            return 1;
        }
        ts.advance(500, MSEC);
    }

    if (!timer_fired) {
        printf("FAIL: timer did not fire when expected\n");
        return 1;
    }

    return 0;
}
