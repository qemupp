#include "timelib.hpp"
#include "util.hpp"

#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

bool check_drift(TestTimeSource *ts, uint64_t frequency, TimeUnit unit,
		 DriftMode mode, int *intcnt)
{
    uint64_t load_time, run_time;
    int target_ints = 50;

    load_time = now(unit);

    run_time = load_time + target_ints * frequency;

    *intcnt = 0;
    while (now(unit) < run_time) {
        if (*intcnt > target_ints) {
            printf("FAIL: unexpected interrupt\n");
            return 1;
        }
        if ((run_time - now(unit)) > ((target_ints * frequency) / 2) &&
            !randint(0, 100)) {
            ts->advance(MIN(run_time - now(unit), frequency * 5), unit);
        } else {
            ts->advance(frequency / 10, unit);
        }
    }

    if (*intcnt != target_ints) {
        printf("%s: expected %d interrupts, got %d\n",
               (mode == DRIFT_DROP) ? "WARN" : "FAIL",
               target_ints, *intcnt);
        if (mode != DRIFT_DROP) {
            return false;
        }
    }
    return true;
}

time_t gettimestamp(void)
{
    time_t timestamp = time(0);
    uint32_t delta;
    int fd;

    fd = open("/dev/urandom", O_RDONLY);
    if (fd == -1) {
        return timestamp;
    }

    if (read(fd, &delta, sizeof(delta)) == sizeof(delta)) {
        delta &= 0x000FFFFF;
        if (delta > 0x7FFFF) {
            timestamp -= (delta - 0x7FFFF);
        } else {
            timestamp += delta;
        }
    }

    close(fd);

    return timestamp;
}
