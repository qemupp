#include "test-time-source.hpp"

#include <stdio.h>

TestTimeSource::TestTimeSource(uint64_t base, TimeUnit unit, bool reliable) :
    time(time_to_ns(base, unit)), reliable(reliable)
{
}

uint64_t TestTimeSource::now(TimeUnit unit)
{
    return ns_to_time(this->time, unit);
}

#include <stdio.h>

void TestTimeSource::arm(Timer *timer)
{
    std::list<Timer *>::iterator i;

    for (i = this->events.begin(); i != this->events.end(); ++i) {
        if (timer->get_deadline() < (*i)->get_deadline()) {
            break;
        }
    }

    this->events.insert(i, timer);
}

void TestTimeSource::cancel(Timer *timer)
{
    std::list<Timer *>::iterator i;

    for (i = this->events.begin(); i != this->events.end(); ++i) {
        if (*i == timer) {
            this->events.erase(i);
            break;
        }
    }
}

void TestTimeSource::advance(uint64_t by, TimeUnit unit)
{
    std::list<Timer *>::iterator i;
    uint64_t target;

    target = time_to_ns(ns_to_time(this->time, unit) + by, unit);

    if (!this->reliable) {
        this->time = target;
    }

    i = this->events.begin();
    while (i != this->events.end() &&
           (*i)->get_deadline() <= target) {
        Timer *timer = *i;
        this->events.pop_front();
        if (this->reliable) {
            if (this->time < timer->get_deadline()) {
                this->time = timer->get_deadline();
            }
        }
        timer->fire();
        i = this->events.begin();
    }

    this->time = target;
}
