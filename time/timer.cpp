#include "timer.hpp"
#include "util.hpp"

uint64_t time_to_ns(uint64_t when, TimeUnit unit)
{
    switch (unit) {
    case SEC:
        return when * 1000000000LL;
    case MSEC:
        return when * 1000000LL;
    case USEC:
        return when * 1000LL;
    case NSEC:
        return when;
    case PC_FREQ0:
        return muldiv64(when, 1000000, 1193182) * 1000LL;
    case PC_FREQ1:
        return muldiv64(when, 1000000, 32768) * 1000LL;
    }

    return when;
}

uint64_t ns_to_time(uint64_t when, TimeUnit unit)
{
    switch (unit) {
    case SEC:
        return when / 1000000000LL;
    case MSEC:
        return when / 1000000LL;
    case USEC:
        return when / 1000LL;
    case NSEC:
        return when;
    case PC_FREQ0:
        return muldiv64(when / 1000LL, 1193182, 1000000);
    case PC_FREQ1:
        return muldiv64(when / 1000LL, 32768, 1000000);
    }

    return when;
}

static TimeSource *global_time_source;

TimeSource *TimeSource::get_instance(void)
{
    return global_time_source;
}

void register_time_source(TimeSource *ts)
{
    global_time_source = ts;
}

Timer::Timer() :
    deadline(0)
{
}

void Timer::set(std::function<void (void)> func)
{
    this->func = func;
}

void Timer::update(uint64_t next_deadline, TimeUnit unit)
{
    this->cancel();
    this->deadline = time_to_ns(next_deadline, unit);

    /* ensure that we don't fire a timer before the deadline in the native units
     * this can happen because of rounding errors.  This is pretty brute force
     * but it should converge in 1-2 iterations.
     */
    for (int i = 1;
         ns_to_time(this->deadline, unit) < next_deadline;
         ++i) {
        this->deadline = time_to_ns(next_deadline + i, unit);
    }
        
    TimeSource::get_instance()->arm(this);
}

void Timer::cancel(void)
{
    TimeSource::get_instance()->cancel(this);
    this->deadline = 0;
}

uint64_t Timer::get_deadline(void)
{
    return this->deadline;
}

void Timer::fire(void)
{
    this->deadline = 0;
    this->func();
}

void Timer::pickle(Marshaller *m, const char *name)
{
    m->start_struct(name, "Timer");
    marshal(m, "deadline", &this->deadline);
    m->end_struct();
}
